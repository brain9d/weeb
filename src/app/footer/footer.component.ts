import { ConfigService } from './../config.service';
import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})

export class FooterComponent implements OnInit {

  copyrightDate;

  constructor(public config: ConfigService) { 
    var d = new Date();
    this.copyrightDate = d.getFullYear();
  }

  ngOnInit() { }

}
