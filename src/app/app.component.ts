import { ConfigService } from './config.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService, LangChangeEvent, TranslationChangeEvent } from '@ngx-translate/core';

import { 
  Router, 
  Event as RouterEvent, 
  NavigationStart, 
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';

declare var jQuery: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  home = 'Home is here';
  isHome: boolean = false;
  isRtl: boolean = false;
  langCode;
  isMobile;
  
  constructor(
    private translate: TranslateService,
    private router: Router,
    public config: ConfigService,
  ) {
      this.isMobile = jQuery(window).width() <= 768 ? true : false;
      translate.setDefaultLang('en-US');
      this.langCode = config.readCookie("lang_code") ? config.readCookie("lang_code") : 'en-US';

      router.events.subscribe((event: RouterEvent) => {
        this.navigationInterceptor(event)
      })

      translate.onLangChange.subscribe((event: LangChangeEvent) => {
        this.config.createCookie("lang_code", event.lang, null);
      });

  }

  ngOnInit() {
    this.config.getLanguage()
    .then(
      (response) => {
        const data = response.data;
        if(data){
          this.config.languages = data;
          const index = data.map(function(e: any) { return e.lang_code; }).indexOf(this.langCode);
          this.config.currentLanguage = data[index];
          this.switchLanguage(this.config.currentLanguage);
        } 
      },
      (errorResponse) => {
        const data = errorResponse.data;
        if( data && data.error && data.error.message) {
        }
      }
    );
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.config.showMainLoader = true;
    }
    
    if (event instanceof NavigationEnd) {

      jQuery("#navbar").collapse("hide");

      setTimeout( () => {
        this.config.showMainLoader = false;
      },500)
      
      if(event.url == '/')
        this.isHome = true;
      else
        this.isHome = false;

      window.scrollTo(0, 0); // to scroll page to top after route change

      // Google Analytics
      (<any>window).ga('set', 'page', event.urlAfterRedirects);
      (<any>window).ga('send', 'pageview');
      // End Google Analytics

      // Facebook Pixel Code
      (<any>window).fbq('track', 'PageView');
      // End Facebook Pixel Code
    }

    if (event instanceof NavigationCancel) {
      this.config.showMainLoader = false
    }

    if (event instanceof NavigationError) {
      this.config.showMainLoader = false
    }
  }

  switchLanguage(language: any) {
    this.translate.use(language.lang_code);
    this.config.currentLanguage = language;
  }

}
