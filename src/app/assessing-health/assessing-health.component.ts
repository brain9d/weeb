import { ConfigService } from './../config.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-assessing-health',
  templateUrl: './assessing-health.component.html',
  styleUrls: ['./assessing-health.component.css']
})
export class AssessingHealthComponent implements OnInit {

  constructor(public config: ConfigService) { }

  ngOnInit() {
  }

}
