import { ConfigService } from './../config.service';
import { Component, OnInit } from '@angular/core';
declare var jQuery: any;

@Component({
  selector: 'app-learning-center',
  templateUrl: './learning-center.component.html',
  styleUrls: ['./learning-center.component.css']
})
export class LearningCenterComponent implements OnInit {
  
  constructor(
    public config: ConfigService
  ) { }

  ngOnInit() {
    jQuery( "#externalContainer" ).load( this.config.externalLinksUrl + "learningcenter/index.php" );
  }

}
