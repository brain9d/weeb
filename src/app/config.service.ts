import { enableProdMode, Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
declare var jQuery:any;
enableProdMode();

@Injectable()
export class ConfigService {
  showLoader: boolean = false;
  showMainLoader: boolean;  
  languages;
  currentLanguage;
  
  server = "brain9d";
  apiDomain = "https://dev.brain9d.com/";
  appDomain = "https://api.app.dev.brain9d.com/v1";
  facebookAppId = "216915902095824";
  forumUrl = "https://app.dev.brain9d.com/forum/index.php";
  androidAppUrl = "https://play.google.com/store/apps/details?id=com.brain9d.brainhealth";
  iphoneAppUrl = "https://itunes.apple.com/us/app/brain9d/id1176918637?mt=8";
  externalLinksUrl = "https://brain9d.com/";
  twoCheckoutSid = '103041325';
  pushNotificationTopic = 'brain9d';
  
  googleAppId = "990135941036-v886ogicnltqaj6hssssk50vls7v0lcb.apps.googleusercontent.com";
  isNativeApp = navigator.userAgent.match(/B9DNativeApp/i) //Detecting Application
  
  constructor(
    private http: Http
  ) { 
    if(window.location.origin.indexOf('dev.') > -1 || window.location.origin.indexOf('localhost') > -1)
      this.server = "development";
    else if(window.location.origin.indexOf('test.') > -1)
      this.server = "test";

    if(this.server === 'development') {
      this.apiDomain = "https://api.app.dev.brain9d.com/v1";
      this.appDomain = "https://app.dev.brain9d.com/";
      this.facebookAppId = "217392898670282";
      this.forumUrl = "https://dev.brain9d.com/forum/index.php";
      this.twoCheckoutSid = '901341537';
      this.pushNotificationTopic = 'brain9d_dev';
    }

    if(this.server === 'test') {
      this.apiDomain = "https://api.test.brain9d.com/v1";
      this.appDomain = "https://app.test.brain9d.com/";
      this.facebookAppId = "487834468280591";
      this.forumUrl = "https://test.brain9d.com/forum/index.php";
      this.twoCheckoutSid = '901341537';
      this.pushNotificationTopic = 'brain9d_dev';
    }

    if(window.location.origin.indexOf('localhost') > -1)
      this.facebookAppId = "370978729918129";
  }

  deferYoutubeVideo() {
    var vidDefer = document.getElementsByTagName('iframe');
    for (var i = 0; i < vidDefer.length; i++) {
      if (vidDefer[i].getAttribute('data-src')) {
        vidDefer[i].setAttribute('src', vidDefer[i].getAttribute('data-src'));
      }
    }
  }

  readCookie = function(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
  }
  
  createCookie = function(name,value,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toUTCString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
  };
  
  eraseCookie = function(name) {
		this.createCookie(name,"",-1);
  };
  
  getLanguage () {
    return this.http.get(this.apiDomain+'/language?is_enable=true')
    .map(
      (response: Response) => {
        const responseObj = {
          data: response.json(),
          headers: response.headers.toJSON(),
          status: response.status
        }
        return responseObj;
      }
    )
    .toPromise()
    .catch(
      (error: Response) => {
        const errorObj = {
          data: error.json(),
          status: error.status,
          statusText: error.statusText,
          url: error.url
        }
        throw(errorObj);
      }
    );
  }

}
