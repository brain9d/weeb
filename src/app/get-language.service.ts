import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class GetLanguageService {

  constructor(private http: Http, public config: ConfigService) {
    this.getLanguage();
  }
  
  getLanguage () {
    return this.http.get(this.config.apiDomain+'/language?is_enable=true')
    .map(
      (response: Response) => {
        const responseObj = {
          data: response.json(),
          headers: response.headers.toJSON(),
          status: response.status
        }
        return responseObj;
      }
    )
    .catch(
      (error: Response) => {
        const errorObj = {
          data: error.json(),
          status: error.status,
          statusText: error.statusText,
          url: error.url
        }
        throw(errorObj);
      }
    );
  }

}
