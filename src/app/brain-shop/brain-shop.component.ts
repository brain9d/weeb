import { ConfigService } from './../config.service';
import { Component, OnInit } from '@angular/core';
declare var jQuery: any;

@Component({
  selector: 'app-brain-shop',
  templateUrl: './brain-shop.component.html',
  styleUrls: ['./brain-shop.component.css']
})
export class BrainShopComponent implements OnInit {
    
  constructor(
    public config: ConfigService
  ) {
      
  }
  
  ngOnInit() {
    jQuery( "#externalContainer" ).load( this.config.externalLinksUrl + "brainshop/" );
  }

  ngAfterContentInit() {
   
  }


}
