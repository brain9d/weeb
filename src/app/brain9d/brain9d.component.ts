import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../config.service';

@Component({
  selector: 'app-brain9d',
  templateUrl: './brain9d.component.html',
  styleUrls: ['./brain9d.component.css']
})
export class Brain9dComponent implements OnInit {

  constructor(
    public config: ConfigService
  ) { }

  ngOnInit() {
  }

}
