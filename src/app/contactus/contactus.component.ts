import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Http, Response } from '@angular/http';
import { NgForm } from '@angular/forms';
import { ConfigService } from '../config.service'

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
  @ViewChild('contactForm') contactForm: NgForm;
  firstInvalid: HTMLElement;
  submittedSuccessfully: boolean;
  addresses = [];

  constructor(
    public config: ConfigService,
    private http: Http
  ) { 
    this.getAddresses();
  }

  ngOnInit() {
  }

  submitForm() {
    this.firstInvalid = <HTMLElement>document.getElementsByClassName("ng-invalid")[1];
    if (this.contactForm.invalid) {
      this.firstInvalid.focus();
    }
    else {
      this.config.showLoader = true;
      this.http.post(this.config.apiDomain+'/users/contactus', {
        "name": this.contactForm.value.username,
        "email": this.contactForm.value.email,
        "subject": this.contactForm.value.subject,
        "message": this.contactForm.value.message,
        "lang_code": "en-USS"
      })
      .subscribe(
        (response: Response) => {
          if(response){
            this.config.showLoader = false;
            this.contactForm.resetForm();
            this.submittedSuccessfully = true;
          }
        },
        (error) => {
          this.config.showLoader = false;
          this.submittedSuccessfully = false;
        }
      );
    }
  }

  getAddresses() {
    this.config.showLoader = true;
    
    this.http.get(this.config.apiDomain+'/contact-detail?visible=1')
    .subscribe(
      (response: Response) => {
        const headers = response.headers;
        this.config.showLoader = false;
        if(response){
          this.addresses = response.json();
        }
      },
      (error) => {
        this.config.showLoader = false;
      }
    );

  };

}
