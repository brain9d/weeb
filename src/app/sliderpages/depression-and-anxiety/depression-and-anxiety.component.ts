import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../config.service';

@Component({
  selector: 'app-depression-and-anxiety',
  templateUrl: './depression-and-anxiety.component.html',
  styleUrls: ['./depression-and-anxiety.component.css']
})
export class DepressionAndAnxietyComponent implements OnInit {

  constructor(
    public config: ConfigService
  ) { }

  ngOnInit() {
  }

}
