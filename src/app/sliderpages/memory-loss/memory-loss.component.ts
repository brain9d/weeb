import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../config.service';

@Component({
  selector: 'app-memory-loss',
  templateUrl: './memory-loss.component.html',
  styleUrls: ['./memory-loss.component.css']
})
export class MemoryLossComponent implements OnInit {

  constructor(
    public config: ConfigService
  ) { }

  ngOnInit() {
  }

}
