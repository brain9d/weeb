import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';

import { ConfigService } from '../config.service'

@Component({
  selector: 'app-feedback-listing',
  templateUrl: './feedback-listing.component.html',
  styleUrls: ['./feedback-listing.component.css']
})
export class FeedbackListingComponent implements OnInit {
  pagination = [];
  feedbacks = [];
  newPage = {
    page_num: 0,
    isCurrent: false
  };
  currentPage; totalPages;
  totalRecords; query;
 
  constructor(
    private http: Http, 
    public config: ConfigService
  ) { }

  ngOnInit() {
    this.getFeedback(1);
  }

  getFeedback(pageNumber) {
    this.config.showLoader = true;

    if(pageNumber == 0 || pageNumber == this.currentPage || pageNumber == this.totalPages+1)
      return;
    this.pagination = [];
    this.query = "?ipp=10&&fbk_status=publish&&expand=feedbackUser";
    if(pageNumber)
      this.query += '&page=' +pageNumber;
    
    this.http.get(this.config.apiDomain+'/feedback'+this.query)
    .subscribe(
      (response: Response) => {
        const headers = response.headers;
        this.config.showLoader = false;
        if(response){
          const domain = this.config.apiDomain.replace("v1", "uploads/");
          this.feedbacks = response.json().map(function(feedback){
            if(feedback.feedbackUser && feedback.feedbackUser.usr_profile_picture)
              feedback.fbk_image = domain + feedback.feedbackUser.usr_profile_picture;
            else
              feedback.fbk_image = "../assets/images/feed_back.png";
            return feedback;
          });
          this.currentPage = parseInt(headers.get('X-Pagination-Current-Page'));
          this.totalPages = parseInt(headers.get('X-Pagination-Page-Count'));
          var recordsPerPage = parseInt(headers.get('X-Pagination-Per-Page'));
          this.totalRecords = parseInt(headers.get('X-Pagination-Total-Count'));
          for( var i =1; i <= this.totalPages; i++ )
          {
            this.newPage = {
              page_num: 0,
              isCurrent: false
            };
            this.newPage.page_num = i;
            this.newPage.isCurrent = this.currentPage == i? true:false;
            this.pagination.push(this.newPage);
          }
        }
      },
      (error) => {
        this.config.showLoader = false;
      }
    );

  };
  
}
