import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Http, Response } from '@angular/http';

import { ConfigService } from '../config.service';
declare var $:any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // Initializations
  showLoader: boolean = true;
  feedbacks: String[] = [];

  constructor(
    private http: Http,
    public config: ConfigService
  ) { }

  ngOnInit() {
    $('#myCarousel').carousel({
      pause: true,
      interval: 6000,
    });
    this.showLoader = true;
    this.getUserFeedback();
    $("a.page-scroll").click(function () {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('data-target')).offset().top
      }, 500);
    });
  }

  getUserFeedback () {
    this.http.get(this.config.apiDomain+'/feedback?ipp=6&&fbk_status=publish&&expand=feedbackUser')
    .subscribe(
      (response: Response) => {
        this.showLoader = false;
        this.feedbacks = response.json().map(function(feedback){
          const domain = 'https://api.dev.brain9d.com/uploads/';
          if(feedback.feedbackUser && feedback.feedbackUser.usr_profile_picture)
            feedback.fbk_image = domain + feedback.feedbackUser.usr_profile_picture;
          else
            feedback.fbk_image = "../assets/images/feed_back.png";
          return feedback;
        });

      },
      (error) => console.log(error)
    );
  }

}
