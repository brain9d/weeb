import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.css']
})
export class TestimonialsComponent implements OnInit {

  // Initializations
  showLoader: boolean = true;
  feedbacks: String[] = [];

  constructor(private http: Http) { }

  ngOnInit() {
    this.showLoader = true;
    // this.getUserFeedback();
  }

  // getUserFeedback () {
  //   this.http.get('https://api.dev.brain9d.com/v1/feedback?ipp=6&&fbk_status=publish&&expand=feedbackUser')
  //   .subscribe(
  //     (response: Response) => {
  //       this.showLoader = false;
  //       this.feedbacks = response.json().map(function(feedback){
  //         const domain = 'https://api.dev.brain9d.com/uploads/';
  //         if(feedback.feedbackUser && feedback.feedbackUser.usr_profile_picture)
  //           feedback.fbk_image = domain + feedback.feedbackUser.usr_profile_picture;
  //         else
  //           feedback.fbk_image = "../assets/images/feed_back.png";
  //         return feedback;
  //       });

  //     },
  //     (error) => console.log(error)
  //   );
  // }

}

