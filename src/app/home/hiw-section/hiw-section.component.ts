import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../config.service';

@Component({
  selector: 'app-hiw-section',
  templateUrl: './hiw-section.component.html',
  styleUrls: ['./hiw-section.component.css']
})
export class HiwSectionComponent implements OnInit {

  constructor(
    private config: ConfigService
  ) { }

  ngOnInit() {}

  ngAfterViewInit() {
    this.config.deferYoutubeVideo();
  }

}
