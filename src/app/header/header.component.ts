import { Component, OnInit, AfterContentChecked, Output, EventEmitter } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { ConfigService } from '../config.service';
declare var jQuery: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})

export class HeaderComponent implements OnInit {
  @Output() changeLanguage = new EventEmitter<string>();
  isHome: boolean = false;
  
  constructor(
    private router: Router, 
    public config: ConfigService
  ) {
    router.events.forEach((event) => {
      if(event instanceof NavigationEnd) {
        if(event.url == '/')
          this.isHome = true;
        else
          this.isHome = false;
      }
    });
  }

  ngOnInit() {
    jQuery("body").on("click", ".dropMenu > a", function () {
      jQuery(this).siblings(".dropdown-menu").toggle();
    });
    jQuery("body").on("click", ".dropMenu .dropdown-menu a", function () {
      jQuery(this).parents(".dropdown-menu").toggle();
    });
  }

  ngAfterViewInit() {
    jQuery("#navbar li:not('.dropMenu') a").on("click", function(){
      jQuery("#navbar").collapse("hide");
    });
  }

  applyLanguageChange(language) {
    const index = this.config.languages.map(function(e: any) { return e.lang_code; }).indexOf(language);
    this.changeLanguage.emit(this.config.languages[index]);

    jQuery("#navbar").collapse("hide");
  }

}

