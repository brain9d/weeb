import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HowItWorksComponent } from './how-it-works/how-it-works.component';
import { HowItWorksResultComponent } from './how-it-works-result/how-it-works-result.component';
import { HomeComponent } from './home/home.component';
import { FaqComponent } from './faq/faq.component';
import { MembershipComponent } from './membership/membership.component';
import { DisclaimerCopyrightsComponent } from './disclaimer-copyrights/disclaimer-copyrights.component';
import { Brain9dComponent } from './brain9d/brain9d.component';
import { FeedbackListingComponent } from './feedback-listing/feedback-listing.component';
import { AssessingHealthComponent } from './assessing-health/assessing-health.component';
import { ContactusComponent } from './contactus/contactus.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { MainLoaderComponent } from './main-loader/main-loader.component';
import { InnerLoaderComponent } from './inner-loader/inner-loader.component';
import { TestimonialsComponent } from './home/testimonials/testimonials.component';
import { HiwSectionComponent } from './home/hiw-section/hiw-section.component';
import { BrainShopComponent } from './brain-shop/brain-shop.component';
import { LearningCenterComponent } from './learning-center/learning-center.component';
import { StressComponent } from './sliderpages/stress/stress.component';
import { DepressionAndAnxietyComponent } from './sliderpages/depression-and-anxiety/depression-and-anxiety.component';
import { MemoryLossComponent } from './sliderpages/memory-loss/memory-loss.component';
import { EmploymentComponent } from './employment/employment.component';
import { BlogComponent } from './blog/blog.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'how-it-works', component: HowItWorksComponent },
    { path: 'how-it-works-result', component: HowItWorksResultComponent },
    { path: 'membership', component: MembershipComponent },
    { path: 'copyrights', component: DisclaimerCopyrightsComponent },
    { path: 'brain9d', component: Brain9dComponent },
    { path: 'terms-and-conditions', component: TermsAndConditionsComponent },
    { path: 'brain-health', component: AssessingHealthComponent },
    { path: 'contact-us', component: ContactusComponent },
    { path: 'faq', component: FaqComponent },
    { path: 'testimonials', component: FeedbackListingComponent },
    { path: 'brain-shop', component: BrainShopComponent },
    { path: 'learning-center', component: LearningCenterComponent },
    { path: 'stress', component: StressComponent },
    { path: 'depression', component: DepressionAndAnxietyComponent },
    { path: 'memory-loss', component: MemoryLossComponent },
    { path: 'employment', component: EmploymentComponent },
    { path: 'blog', component: BlogComponent }
]

@NgModule({
    imports:[
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {}