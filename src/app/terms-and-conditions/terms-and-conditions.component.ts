import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
declare var $:any;
@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.css']
})
export class TermsAndConditionsComponent implements OnInit {

  constructor(private route: ActivatedRoute,private router: Router) { }

  ngOnInit() {
    $("a#anchorEU").click(function () {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('data-target')).offset().top
      }, 500);
    });
  }
}
