// Modules
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Services
import { ConfigService } from './config.service';
import { GetLanguageService } from './get-language.service';

// Components
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HowItWorksComponent } from './how-it-works/how-it-works.component';
import { HowItWorksResultComponent } from './how-it-works-result/how-it-works-result.component';
import { HomeComponent } from './home/home.component';
import { FaqComponent } from './faq/faq.component';
import { MembershipComponent } from './membership/membership.component';
import { DisclaimerCopyrightsComponent } from './disclaimer-copyrights/disclaimer-copyrights.component';
import { Brain9dComponent } from './brain9d/brain9d.component';
import { FeedbackListingComponent } from './feedback-listing/feedback-listing.component';
import { AssessingHealthComponent } from './assessing-health/assessing-health.component';
import { ContactusComponent } from './contactus/contactus.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { MainLoaderComponent } from './main-loader/main-loader.component';
import { InnerLoaderComponent } from './inner-loader/inner-loader.component';
import { TestimonialsComponent } from './home/testimonials/testimonials.component';
import { HiwSectionComponent } from './home/hiw-section/hiw-section.component';
import { FooterComponent } from './footer/footer.component';
import { BrainShopComponent } from './brain-shop/brain-shop.component';
import { LearningCenterComponent } from './learning-center/learning-center.component';
import { AdsCtaComponent } from './home/ads-cta/ads-cta.component';
import { PostAdComponent } from './home/post-ad/post-ad.component';
import { StressComponent } from './sliderpages/stress/stress.component';
import { DepressionAndAnxietyComponent } from './sliderpages/depression-and-anxiety/depression-and-anxiety.component';
import { MemoryLossComponent } from './sliderpages/memory-loss/memory-loss.component';
import { Brain9dSectionComponent } from './home/brain9d-section/brain9d-section.component';
import { GetStartedComponent } from './home/get-started/get-started.component';
import { EmploymentComponent } from './employment/employment.component';
import { BlogComponent } from './blog/blog.component';

var apiDomain;
export function HttpLoaderFactory(https: HttpClient) {
  return new TranslateHttpLoader(https, apiDomain+"/translation/read-file?lang_code=", "");
  // return new TranslateHttpLoader(https, "/assets/i18n/", ".json");
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HowItWorksComponent,
    HowItWorksResultComponent,
    HomeComponent,
    FaqComponent,
    MembershipComponent,
    DisclaimerCopyrightsComponent,
    Brain9dComponent,
    FeedbackListingComponent,
    AssessingHealthComponent,
    ContactusComponent,
    TermsAndConditionsComponent,
    MainLoaderComponent,
    InnerLoaderComponent,
    TestimonialsComponent,
    HiwSectionComponent,
    FooterComponent,
    BrainShopComponent,
    LearningCenterComponent,
    AdsCtaComponent,
    PostAdComponent,
    StressComponent,
    DepressionAndAnxietyComponent,
    MemoryLossComponent,
    Brain9dSectionComponent,
    GetStartedComponent,
    EmploymentComponent,
    BlogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    })
  ],
  providers: [ConfigService, GetLanguageService],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(private config: ConfigService){
    apiDomain = config.apiDomain;
  }
}
