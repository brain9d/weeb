webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__how_it_works_how_it_works_component__ = __webpack_require__("../../../../../src/app/how-it-works/how-it-works.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__how_it_works_result_how_it_works_result_component__ = __webpack_require__("../../../../../src/app/how-it-works-result/how-it-works-result.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home_component__ = __webpack_require__("../../../../../src/app/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__faq_faq_component__ = __webpack_require__("../../../../../src/app/faq/faq.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__membership_membership_component__ = __webpack_require__("../../../../../src/app/membership/membership.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__disclaimer_copyrights_disclaimer_copyrights_component__ = __webpack_require__("../../../../../src/app/disclaimer-copyrights/disclaimer-copyrights.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__brain9d_brain9d_component__ = __webpack_require__("../../../../../src/app/brain9d/brain9d.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__feedback_listing_feedback_listing_component__ = __webpack_require__("../../../../../src/app/feedback-listing/feedback-listing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assessing_health_assessing_health_component__ = __webpack_require__("../../../../../src/app/assessing-health/assessing-health.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__contactus_contactus_component__ = __webpack_require__("../../../../../src/app/contactus/contactus.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__terms_and_conditions_terms_and_conditions_component__ = __webpack_require__("../../../../../src/app/terms-and-conditions/terms-and-conditions.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__brain_shop_brain_shop_component__ = __webpack_require__("../../../../../src/app/brain-shop/brain-shop.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__learning_center_learning_center_component__ = __webpack_require__("../../../../../src/app/learning-center/learning-center.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__sliderpages_stress_stress_component__ = __webpack_require__("../../../../../src/app/sliderpages/stress/stress.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__sliderpages_depression_and_anxiety_depression_and_anxiety_component__ = __webpack_require__("../../../../../src/app/sliderpages/depression-and-anxiety/depression-and-anxiety.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__sliderpages_memory_loss_memory_loss_component__ = __webpack_require__("../../../../../src/app/sliderpages/memory-loss/memory-loss.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__employment_employment_component__ = __webpack_require__("../../../../../src/app/employment/employment.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__blog_blog_component__ = __webpack_require__("../../../../../src/app/blog/blog.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_4__home_home_component__["a" /* HomeComponent */] },
    { path: 'how-it-works', component: __WEBPACK_IMPORTED_MODULE_2__how_it_works_how_it_works_component__["a" /* HowItWorksComponent */] },
    { path: 'how-it-works-result', component: __WEBPACK_IMPORTED_MODULE_3__how_it_works_result_how_it_works_result_component__["a" /* HowItWorksResultComponent */] },
    { path: 'membership', component: __WEBPACK_IMPORTED_MODULE_6__membership_membership_component__["a" /* MembershipComponent */] },
    { path: 'copyrights', component: __WEBPACK_IMPORTED_MODULE_7__disclaimer_copyrights_disclaimer_copyrights_component__["a" /* DisclaimerCopyrightsComponent */] },
    { path: 'brain9d', component: __WEBPACK_IMPORTED_MODULE_8__brain9d_brain9d_component__["a" /* Brain9dComponent */] },
    { path: 'terms-and-conditions', component: __WEBPACK_IMPORTED_MODULE_12__terms_and_conditions_terms_and_conditions_component__["a" /* TermsAndConditionsComponent */] },
    { path: 'brain-health', component: __WEBPACK_IMPORTED_MODULE_10__assessing_health_assessing_health_component__["a" /* AssessingHealthComponent */] },
    { path: 'contact-us', component: __WEBPACK_IMPORTED_MODULE_11__contactus_contactus_component__["a" /* ContactusComponent */] },
    { path: 'faq', component: __WEBPACK_IMPORTED_MODULE_5__faq_faq_component__["a" /* FaqComponent */] },
    { path: 'testimonials', component: __WEBPACK_IMPORTED_MODULE_9__feedback_listing_feedback_listing_component__["a" /* FeedbackListingComponent */] },
    { path: 'brain-shop', component: __WEBPACK_IMPORTED_MODULE_13__brain_shop_brain_shop_component__["a" /* BrainShopComponent */] },
    { path: 'learning-center', component: __WEBPACK_IMPORTED_MODULE_14__learning_center_learning_center_component__["a" /* LearningCenterComponent */] },
    { path: 'stress', component: __WEBPACK_IMPORTED_MODULE_15__sliderpages_stress_stress_component__["a" /* StressComponent */] },
    { path: 'depression', component: __WEBPACK_IMPORTED_MODULE_16__sliderpages_depression_and_anxiety_depression_and_anxiety_component__["a" /* DepressionAndAnxietyComponent */] },
    { path: 'memory-loss', component: __WEBPACK_IMPORTED_MODULE_17__sliderpages_memory_loss_memory_loss_component__["a" /* MemoryLossComponent */] },
    { path: 'employment', component: __WEBPACK_IMPORTED_MODULE_18__employment_employment_component__["a" /* EmploymentComponent */] },
    { path: 'blog', component: __WEBPACK_IMPORTED_MODULE_19__blog_blog_component__["a" /* BlogComponent */] }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgModule */])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forRoot(appRoutes)
        ],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */]]
    })
], AppRoutingModule);

//# sourceMappingURL=app-routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div [ngClass]=\"{'home' : isHome , 'notHome': !isHome, 'body': true, 'mobile': isMobile, 'rtl': config.currentLanguage?.lang_is_rtl}\">\r\n\r\n    <div class=\"push-notification-wrapper\"></div>\r\n\r\n    <!-- Main Loader -->\r\n    <app-main-loader *ngIf=\"config.showMainLoader\"></app-main-loader>\r\n\r\n    <!-- Modal shown to refer app to customers -->\r\n    <div id=\"referAppModal\" class=\"modal\">\r\n        <div class=\"modal-dialog\">\r\n            <div class=\"modal-content\">\r\n                <div class=\"modal-header\">\r\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\r\n                    <h3 translate=\"miscellaneous.trybrain9dapp\"></h3>\r\n                </div>\r\n                <div class=\"modal-body text-center\">\r\n                    <span translate=\"miscellaneous.tryappmessage\"></span>\r\n                    <br><br>\r\n                    <!-- <button type=\"button\" class=\"btn custom-small-btn btn-primary\" (click)=\"appDeepLink()\" data-dismiss=\"modal\" translate=\"buttons.takemetotheapp\"></button> -->\r\n                    <button type=\"button\" class=\"btn custom-small-btn btn-primary\" data-dismiss=\"modal\" translate=\"buttons.takemetotheapp\"></button>\r\n                    <p style=\"margin-top: 15px;text-align: center;margin-bottom: -10px;\" *ngIf=\"config.iphoneAppUrl\">\r\n                        <span translate=\"miscellaneous.donothaveiosapp\"></span>\r\n                        <br>\r\n                        <a style=\"text-decoration:underline\" href=\"{{config.iphoneAppUrl}}\" translate=\"buttons.gotoapplestore\"></a>\r\n                    </p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <!-- Navigation Menu -->\r\n    <app-header (changeLanguage)=\"switchLanguage($event)\"></app-header>\r\n\r\n    <!-- Main content of all pages go here -->\r\n    <div class=\"close-menu\" [ngClass]=\"{'main-area': !isHome , 'home-slider': isHome}\" id=\"main-view\">\r\n        <app-inner-loader *ngIf=\"config.showLoader\"></app-inner-loader>\r\n        <router-outlet></router-outlet>\r\n    </div>\r\n\r\n    <div class=\"clearfix\"></div>\r\n\r\n    <!-- App Footer -->\r\n    <app-footer></app-footer>\r\n\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = (function () {
    function AppComponent(translate, router, config) {
        var _this = this;
        this.translate = translate;
        this.router = router;
        this.config = config;
        this.home = 'Home is here';
        this.isHome = false;
        this.isRtl = false;
        this.isMobile = jQuery(window).width() <= 768 ? true : false;
        translate.setDefaultLang('en-US');
        this.langCode = config.readCookie("lang_code") ? config.readCookie("lang_code") : 'en-US';
        router.events.subscribe(function (event) {
            _this.navigationInterceptor(event);
        });
        translate.onLangChange.subscribe(function (event) {
            _this.config.createCookie("lang_code", event.lang, null);
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.config.getLanguage()
            .then(function (response) {
            var data = response.data;
            if (data) {
                _this.config.languages = data;
                var index = data.map(function (e) { return e.lang_code; }).indexOf(_this.langCode);
                _this.config.currentLanguage = data[index];
                _this.switchLanguage(_this.config.currentLanguage);
            }
        }, function (errorResponse) {
            var data = errorResponse.data;
            if (data && data.error && data.error.message) {
            }
        });
    };
    AppComponent.prototype.navigationInterceptor = function (event) {
        var _this = this;
        if (event instanceof __WEBPACK_IMPORTED_MODULE_3__angular_router__["e" /* NavigationStart */]) {
            this.config.showMainLoader = true;
        }
        if (event instanceof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* NavigationEnd */]) {
            jQuery("#navbar").collapse("hide");
            setTimeout(function () {
                _this.config.showMainLoader = false;
            }, 500);
            if (event.url == '/')
                this.isHome = true;
            else
                this.isHome = false;
            window.scrollTo(0, 0); // to scroll page to top after route change
            // Google Analytics
            window.ga('set', 'page', event.urlAfterRedirects);
            window.ga('send', 'pageview');
            // End Google Analytics
            // Facebook Pixel Code
            window.fbq('track', 'PageView');
            // End Facebook Pixel Code
        }
        if (event instanceof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* NavigationCancel */]) {
            this.config.showMainLoader = false;
        }
        if (event instanceof __WEBPACK_IMPORTED_MODULE_3__angular_router__["d" /* NavigationError */]) {
            this.config.showMainLoader = false;
        }
    };
    AppComponent.prototype.switchLanguage = function (language) {
        this.translate.use(language.lang_code);
        this.config.currentLanguage = language;
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["o" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["f" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["f" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */]) === "function" && _c || Object])
], AppComponent);

var _a, _b, _c;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export HttpLoaderFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__("../../../common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__("../../../../@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_http_loader__ = __webpack_require__("../../../../@ngx-translate/http-loader/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__get_language_service__ = __webpack_require__("../../../../../src/app/get-language.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__header_header_component__ = __webpack_require__("../../../../../src/app/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__how_it_works_how_it_works_component__ = __webpack_require__("../../../../../src/app/how-it-works/how-it-works.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__how_it_works_result_how_it_works_result_component__ = __webpack_require__("../../../../../src/app/how-it-works-result/how-it-works-result.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__home_home_component__ = __webpack_require__("../../../../../src/app/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__faq_faq_component__ = __webpack_require__("../../../../../src/app/faq/faq.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__membership_membership_component__ = __webpack_require__("../../../../../src/app/membership/membership.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__disclaimer_copyrights_disclaimer_copyrights_component__ = __webpack_require__("../../../../../src/app/disclaimer-copyrights/disclaimer-copyrights.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__brain9d_brain9d_component__ = __webpack_require__("../../../../../src/app/brain9d/brain9d.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__feedback_listing_feedback_listing_component__ = __webpack_require__("../../../../../src/app/feedback-listing/feedback-listing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__assessing_health_assessing_health_component__ = __webpack_require__("../../../../../src/app/assessing-health/assessing-health.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__contactus_contactus_component__ = __webpack_require__("../../../../../src/app/contactus/contactus.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__terms_and_conditions_terms_and_conditions_component__ = __webpack_require__("../../../../../src/app/terms-and-conditions/terms-and-conditions.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__main_loader_main_loader_component__ = __webpack_require__("../../../../../src/app/main-loader/main-loader.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__inner_loader_inner_loader_component__ = __webpack_require__("../../../../../src/app/inner-loader/inner-loader.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__home_testimonials_testimonials_component__ = __webpack_require__("../../../../../src/app/home/testimonials/testimonials.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__home_hiw_section_hiw_section_component__ = __webpack_require__("../../../../../src/app/home/hiw-section/hiw-section.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__footer_footer_component__ = __webpack_require__("../../../../../src/app/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__brain_shop_brain_shop_component__ = __webpack_require__("../../../../../src/app/brain-shop/brain-shop.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__learning_center_learning_center_component__ = __webpack_require__("../../../../../src/app/learning-center/learning-center.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__home_ads_cta_ads_cta_component__ = __webpack_require__("../../../../../src/app/home/ads-cta/ads-cta.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__home_post_ad_post_ad_component__ = __webpack_require__("../../../../../src/app/home/post-ad/post-ad.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__sliderpages_stress_stress_component__ = __webpack_require__("../../../../../src/app/sliderpages/stress/stress.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__sliderpages_depression_and_anxiety_depression_and_anxiety_component__ = __webpack_require__("../../../../../src/app/sliderpages/depression-and-anxiety/depression-and-anxiety.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__sliderpages_memory_loss_memory_loss_component__ = __webpack_require__("../../../../../src/app/sliderpages/memory-loss/memory-loss.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__home_brain9d_section_brain9d_section_component__ = __webpack_require__("../../../../../src/app/home/brain9d-section/brain9d-section.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__home_get_started_get_started_component__ = __webpack_require__("../../../../../src/app/home/get-started/get-started.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__employment_employment_component__ = __webpack_require__("../../../../../src/app/employment/employment.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__blog_blog_component__ = __webpack_require__("../../../../../src/app/blog/blog.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Modules








// Services


// Components





























var apiDomain;
function HttpLoaderFactory(https) {
    return new __WEBPACK_IMPORTED_MODULE_7__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](https, apiDomain + "/translation/read-file?lang_code=", "");
    // return new TranslateHttpLoader(https, "/assets/i18n/", ".json");
}
var AppModule = (function () {
    function AppModule(config) {
        this.config = config;
        apiDomain = config.apiDomain;
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["M" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_11__header_header_component__["a" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_12__how_it_works_how_it_works_component__["a" /* HowItWorksComponent */],
            __WEBPACK_IMPORTED_MODULE_13__how_it_works_result_how_it_works_result_component__["a" /* HowItWorksResultComponent */],
            __WEBPACK_IMPORTED_MODULE_14__home_home_component__["a" /* HomeComponent */],
            __WEBPACK_IMPORTED_MODULE_15__faq_faq_component__["a" /* FaqComponent */],
            __WEBPACK_IMPORTED_MODULE_16__membership_membership_component__["a" /* MembershipComponent */],
            __WEBPACK_IMPORTED_MODULE_17__disclaimer_copyrights_disclaimer_copyrights_component__["a" /* DisclaimerCopyrightsComponent */],
            __WEBPACK_IMPORTED_MODULE_18__brain9d_brain9d_component__["a" /* Brain9dComponent */],
            __WEBPACK_IMPORTED_MODULE_19__feedback_listing_feedback_listing_component__["a" /* FeedbackListingComponent */],
            __WEBPACK_IMPORTED_MODULE_20__assessing_health_assessing_health_component__["a" /* AssessingHealthComponent */],
            __WEBPACK_IMPORTED_MODULE_21__contactus_contactus_component__["a" /* ContactusComponent */],
            __WEBPACK_IMPORTED_MODULE_22__terms_and_conditions_terms_and_conditions_component__["a" /* TermsAndConditionsComponent */],
            __WEBPACK_IMPORTED_MODULE_23__main_loader_main_loader_component__["a" /* MainLoaderComponent */],
            __WEBPACK_IMPORTED_MODULE_24__inner_loader_inner_loader_component__["a" /* InnerLoaderComponent */],
            __WEBPACK_IMPORTED_MODULE_25__home_testimonials_testimonials_component__["a" /* TestimonialsComponent */],
            __WEBPACK_IMPORTED_MODULE_26__home_hiw_section_hiw_section_component__["a" /* HiwSectionComponent */],
            __WEBPACK_IMPORTED_MODULE_27__footer_footer_component__["a" /* FooterComponent */],
            __WEBPACK_IMPORTED_MODULE_28__brain_shop_brain_shop_component__["a" /* BrainShopComponent */],
            __WEBPACK_IMPORTED_MODULE_29__learning_center_learning_center_component__["a" /* LearningCenterComponent */],
            __WEBPACK_IMPORTED_MODULE_30__home_ads_cta_ads_cta_component__["a" /* AdsCtaComponent */],
            __WEBPACK_IMPORTED_MODULE_31__home_post_ad_post_ad_component__["a" /* PostAdComponent */],
            __WEBPACK_IMPORTED_MODULE_32__sliderpages_stress_stress_component__["a" /* StressComponent */],
            __WEBPACK_IMPORTED_MODULE_33__sliderpages_depression_and_anxiety_depression_and_anxiety_component__["a" /* DepressionAndAnxietyComponent */],
            __WEBPACK_IMPORTED_MODULE_34__sliderpages_memory_loss_memory_loss_component__["a" /* MemoryLossComponent */],
            __WEBPACK_IMPORTED_MODULE_35__home_brain9d_section_brain9d_section_component__["a" /* Brain9dSectionComponent */],
            __WEBPACK_IMPORTED_MODULE_36__home_get_started_get_started_component__["a" /* GetStartedComponent */],
            __WEBPACK_IMPORTED_MODULE_37__employment_employment_component__["a" /* EmploymentComponent */],
            __WEBPACK_IMPORTED_MODULE_38__blog_blog_component__["a" /* BlogComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_0__app_routing_module__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                loader: {
                    provide: __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["a" /* TranslateLoader */],
                    useFactory: HttpLoaderFactory,
                    deps: [__WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */]]
                }
            })
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_8__config_service__["a" /* ConfigService */], __WEBPACK_IMPORTED_MODULE_9__get_language_service__["a" /* GetLanguageService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_8__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], AppModule);

var _a;
//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/assessing-health/assessing-health.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/assessing-health/assessing-health.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- main body section -->\r\n<div class=\"main-content-area container am-health\">\r\n\r\n    <h3 class=\"faq-heading\" translate=\"miscellaneous.assessinghealth\"></h3>\r\n\r\n    <div class=\"row margin0\">\r\n\r\n        <div class=\"col-sm-12 am-health-section\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-3 col-md-2 text-center\">\r\n                    <img src=\"../assets/images/dashboard/brain-and-mental.png\" alt=\"Brain and Mental\" />\r\n                </div>\r\n                <div class=\"col-sm-6 col-md-8 desc\">\r\n                    <h3 class=\"am-heading\" translate=\"amhealth.brainandmental\"></h3>\r\n                    <p translate=\"amhealth.brainandmentaldesc\"></p>\r\n                </div>\r\n                <div class=\"col-sm-3 col-md-2 btn-container\">\r\n                    <a [href]=\"config.appDomain+'brain-and-mental'\" target=\"_blank\" class=\"btn btn-primary\"><span translate=\"buttons.trynow\"></span> <span class=\"fa fa-angle-right\"></span></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 am-health-section\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-3 col-md-2 text-center\">\r\n                    <img src=\"../assets/images/dashboard/mood-and-behaviour.png\" alt=\"Mood and Behaviour\" />\r\n                </div>\r\n                <div class=\"col-sm-6 col-md-8 desc\">\r\n                    <h3 class=\"am-heading\" translate=\"amhealth.moodandbehavioural\"></h3>\r\n                    <p translate=\"amhealth.moodandbehaviouraldesc\"></p>\r\n                </div>\r\n                <div class=\"col-sm-3 col-md-2 btn-container\">\r\n                    <a [href]=\"config.appDomain+'mood-and-behavioral'\" target=\"_blank\" class=\"btn btn-primary\"><span translate=\"buttons.trynow\"></span> <span class=\"fa fa-angle-right\"></span></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 am-health-section\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-3 col-md-2 text-center\">\r\n                    <img src=\"../assets/images/dashboard/analyze-from-face.png\" alt=\"Scan your face\" />\r\n                </div>\r\n                <div class=\"col-sm-6 col-md-8 desc\">\r\n                    <h3 class=\"am-heading\" translate=\"amhealth.scanface\"></h3>\r\n                    <p translate=\"amhealth.scanfacedesc\"></p>\r\n                </div>\r\n                <div class=\"col-sm-3 col-md-2 btn-container\">\r\n                    <a [href]=\"config.appDomain+'scan-your-face'\" target=\"_blank\" class=\"btn btn-primary\"><span translate=\"buttons.trynow\"></span> <span class=\"fa fa-angle-right\"></span></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 am-health-section\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-3 col-md-2 text-center\">\r\n                    <img src=\"../assets/images/dashboard/brain-tests.png\" alt=\"Brain Tests\" />\r\n                </div>\r\n                <div class=\"col-sm-6 col-md-8 desc\">\r\n                    <h3 class=\"am-heading\" translate=\"amhealth.braintests\"></h3>\r\n                    <p translate=\"amhealth.braintestsdesc\"></p>\r\n                </div>\r\n                <div class=\"col-sm-3 col-md-2 btn-container\">\r\n                    <a [href]=\"config.appDomain+'brain-tests'\" target=\"_blank\" class=\"btn btn-primary\"><span translate=\"buttons.trynow\"></span> <span class=\"fa fa-angle-right\"></span></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 am-health-section\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-3 col-md-2 text-center\">\r\n                    <img src=\"../assets/images/dashboard/brain-signals.png\" alt=\"Brain Signals\" />\r\n                </div>\r\n                <div class=\"col-sm-6 col-md-8 desc\">\r\n                    <h3 class=\"am-heading\" translate=\"amhealth.brainsignals\"></h3>\r\n                    <p translate=\"amhealth.brainsignalsdesc\"></p>\r\n                </div>\r\n                <div class=\"col-sm-3 col-md-2 btn-container\">\r\n                    <a [href]=\"config.appDomain+'analyze-brain-signals'\" target=\"_blank\" class=\"btn btn-primary\"><span translate=\"buttons.trynow\"></span> <span class=\"fa fa-angle-right\"></span></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n    <h3 class=\"faq-heading\" translate=\"miscellaneous.managinghealth\"></h3>\r\n\r\n    <div class=\"row margin0\">\r\n\r\n        <div class=\"col-sm-12 am-health-section\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-3 col-md-2 text-center\">\r\n                    <img src=\"../assets/images/dashboard/brain-training.png\" alt=\"Brain Training\" />\r\n                </div>\r\n                <div class=\"col-sm-6 col-md-8 desc\">\r\n                    <h3 class=\"am-heading\" translate=\"amhealth.braintraining\"></h3>\r\n                    <p translate=\"amhealth.braintrainingdesc\"></p>\r\n                </div>\r\n                <div class=\"col-sm-3 col-md-2 btn-container\">\r\n                    <a [href]=\"config.appDomain+'brain-training'\" target=\"_blank\" class=\"btn btn-primary\"><span translate=\"buttons.trynow\"></span> <span class=\"fa fa-angle-right\"></span></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 am-health-section\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-3 col-md-2 text-center\">\r\n                    <img src=\"../assets/images/dashboard/find-doctor.png\" alt=\"Find a Doctor\" />\r\n                </div>\r\n                <div class=\"col-sm-6 col-md-8 desc\">\r\n                    <h3 class=\"am-heading\" translate=\"amhealth.finddoctor\"></h3>\r\n                    <p translate=\"amhealth.finddoctordesc\"></p>\r\n                </div>\r\n                <div class=\"col-sm-3 col-md-2 btn-container\">\r\n                    <a [href]=\"config.appDomain+'hospitals'\" target=\"_blank\" class=\"btn btn-primary\"><span translate=\"buttons.trynow\"></span> <span class=\"fa fa-angle-right\"></span></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 am-health-section\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-3 col-md-2 text-center\">\r\n                    <img src=\"../assets/images/dashboard/learning-center.png\" alt=\"Learning Center\" />\r\n                </div>\r\n                <div class=\"col-sm-6 col-md-8 desc\">\r\n                    <h3 class=\"am-heading\" translate=\"amhealth.learningcenter\"></h3>\r\n                    <p translate=\"amhealth.learningcenterdesc\"></p>\r\n                </div>\r\n                <div class=\"col-sm-3 col-md-2 btn-container\">\r\n                    <a [href]=\"config.appDomain+'learning-center'\" target=\"_blank\" class=\"btn btn-primary\"><span translate=\"buttons.trynow\"></span> <span class=\"fa fa-angle-right\"></span></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 am-health-section\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-3 col-md-2 text-center\">\r\n                    <img src=\"../assets/images/dashboard/brain-shop.png\" alt=\"Brain Shop\" />\r\n                </div>\r\n                <div class=\"col-sm-6 col-md-8 desc\">\r\n                    <h3 class=\"am-heading\" translate=\"amhealth.brainshop\"></h3>\r\n                    <p translate=\"amhealth.brainshopdesc\"></p>\r\n                </div>\r\n                <div class=\"col-sm-3 col-md-2 btn-container\">\r\n                    <a [href]=\"config.appDomain+'brain-shop'\" target=\"_blank\" class=\"btn btn-primary\"><span translate=\"buttons.trynow\"></span> <span class=\"fa fa-angle-right\"></span></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 am-health-section\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-3 col-md-2 text-center\">\r\n                    <img src=\"../assets/images/dashboard/support-groups.png\" alt=\"Support Groups\" />\r\n                </div>\r\n                <div class=\"col-sm-6 col-md-8 desc\">\r\n                    <h3 class=\"am-heading\" translate=\"amhealth.supportgroups\"></h3>\r\n                    <p translate=\"amhealth.supportgroupsdesc\"></p>\r\n                </div>\r\n                <div class=\"col-sm-3 col-md-2 btn-container\">\r\n                    <a [href]=\"config.appDomain+'support-groups'\" target=\"_blank\" class=\"btn btn-primary\"><span translate=\"buttons.trynow\"></span> <span class=\"fa fa-angle-right\"></span></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"col-sm-12 am-health-section\">\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-3 col-md-2 text-center\">\r\n                    <img src=\"../assets/images/dashboard/demographic.png\" alt=\"Demographics\" />\r\n                </div>\r\n                <div class=\"col-sm-6 col-md-8 desc\">\r\n                    <h3 class=\"am-heading\" translate=\"amhealth.demographics\"></h3>\r\n                    <p translate=\"amhealth.demographicsdesc\"></p>\r\n                </div>\r\n                <div class=\"col-sm-3 col-md-2 btn-container\">\r\n                    <a [href]=\"config.appDomain+'demograph'\" target=\"_blank\" class=\"btn btn-primary\"><span translate=\"buttons.trynow\"></span> <span class=\"fa fa-angle-right\"></span></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/assessing-health/assessing-health.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AssessingHealthComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AssessingHealthComponent = (function () {
    function AssessingHealthComponent(config) {
        this.config = config;
    }
    AssessingHealthComponent.prototype.ngOnInit = function () {
    };
    return AssessingHealthComponent;
}());
AssessingHealthComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["o" /* Component */])({
        selector: 'app-assessing-health',
        template: __webpack_require__("../../../../../src/app/assessing-health/assessing-health.component.html"),
        styles: [__webpack_require__("../../../../../src/app/assessing-health/assessing-health.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], AssessingHealthComponent);

var _a;
//# sourceMappingURL=assessing-health.component.js.map

/***/ }),

/***/ "../../../../../src/app/blog/blog.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/blog/blog.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-area\">\r\n  <div class=\"container-fluid user-account-container\">\r\n    <div class=\"row\">\r\n\r\n      <!-- <div id=\"externalContainer\" class=\"text-center\">\r\n      </div> -->\r\n\r\n      <iframe src=\"https://brain9d.blogspot.com/\" style=\"width: 100%; height: 700px;\"></iframe>\r\n\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/blog/blog.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlogComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BlogComponent = (function () {
    function BlogComponent(config) {
        this.config = config;
    }
    BlogComponent.prototype.ngOnInit = function () {
        // jQuery( "#externalContainer" ).load( this.config.externalLinksUrl + "brainshop/" );
    };
    BlogComponent.prototype.ngAfterContentInit = function () {
    };
    return BlogComponent;
}());
BlogComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["o" /* Component */])({
        selector: 'blog',
        template: __webpack_require__("../../../../../src/app/blog/blog.component.html"),
        styles: [__webpack_require__("../../../../../src/app/blog/blog.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], BlogComponent);

var _a;
//# sourceMappingURL=blog.component.js.map

/***/ }),

/***/ "../../../../../src/app/brain-shop/brain-shop.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/brain-shop/brain-shop.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-area\">\r\n  <div class=\"container-fluid user-account-container\">\r\n    <div class=\"row\">\r\n        \r\n      <div id=\"externalContainer\" class=\"text-center\">\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n  "

/***/ }),

/***/ "../../../../../src/app/brain-shop/brain-shop.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BrainShopComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BrainShopComponent = (function () {
    function BrainShopComponent(config) {
        this.config = config;
    }
    BrainShopComponent.prototype.ngOnInit = function () {
        jQuery("#externalContainer").load(this.config.externalLinksUrl + "brainshop/");
    };
    BrainShopComponent.prototype.ngAfterContentInit = function () {
    };
    return BrainShopComponent;
}());
BrainShopComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["o" /* Component */])({
        selector: 'app-brain-shop',
        template: __webpack_require__("../../../../../src/app/brain-shop/brain-shop.component.html"),
        styles: [__webpack_require__("../../../../../src/app/brain-shop/brain-shop.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], BrainShopComponent);

var _a;
//# sourceMappingURL=brain-shop.component.js.map

/***/ }),

/***/ "../../../../../src/app/brain9d/brain9d.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".sub-heading {\r\n    font-weight: 600;\r\n    font-style: italic;\r\n    margin-bottom: 30px;\r\n    font-size: 24px;\r\n}\r\n.sub-heading:before, .sub-heading:after {\r\n    content: '\"';\r\n}\r\np.font-italic {\r\n    font-style: italic;\r\n}\r\n.brain9d-page .desc {\r\n    padding-top: 25px;\r\n}\r\n.brain9d-page p {\r\n    font-size: 18px;\r\n    text-align: justify;\r\n    margin-bottom: 25px;\r\n}\r\n.brain9d-page p.text-center {\r\n    text-align: center;\r\n}\r\n.brain9d-page p.special-note {\r\n    color: #00acb8;\r\n    text-align: center;\r\n    font-weight: 600;\r\n}\r\n.brain9d-page .desc ul {\r\n    padding-left: 25px;\r\n    padding-right: 25px;\r\n}\r\n.brain9d-page .desc ul li {\r\n    font-size: 18px;\r\n    font-weight: 600;\r\n    line-height: 25px;\r\n    position: relative;\r\n}\r\n.brain9d-page .desc ul li > i {\r\n    position: absolute;\r\n    left: -25px;\r\n    color: #00acb8;\r\n    top: 3px;\r\n}\r\n.brain9d-page .desc .point {\r\n    background-color: #eee;\r\n    padding: 15px 25px;\r\n    margin-top: 25px;\r\n    font-size: 16px;\r\n    line-height: 25px;\r\n    min-width: 725px;\r\n}\r\n.brain9d-page .page-note {\r\n    font-size: 18px;\r\n    padding: 10px 10px;\r\n    background-color: #f3f3f3;\r\n    border: solid 2px #ccc;\r\n    border-radius: 7px;\r\n    box-shadow: 0 0 6px #999;\r\n    position: relative;\r\n}\r\n.brain9d-page .page-note > .note {\r\n    text-align: justify;\r\n    padding-left: 65px;\r\n}\r\n.brain9d-page .page-note > .note:before {\r\n    content: '';\r\n    position: absolute;\r\n    left: 10px;\r\n    top: 10px;\r\n    bottom: 10px;\r\n    width: 53px;\r\n    background-image: url(" + __webpack_require__("../../../../../src/assets/images/brain9d/note.png") + ");\r\n    background-repeat: no-repeat;\r\n    background-size: cover; \r\n}\r\n.brain9d-page .page-note {\r\n    margin: 0;\r\n}\r\n.brain9d-page .page-note .img {\r\n    text-align: center;\r\n    width: 50px;\r\n    float: left;\r\n}\r\n.brain9d-page .page-note .img > img {\r\n    min-width: 50px;\r\n}\r\n.brain9d-page .page-note .text {\r\n    width: calc(100% - 50px);\r\n    float: left;\r\n    padding: 0 15px;\r\n}\r\n.brain9d-page .page-note .text p {\r\n    margin-bottom: 0;\r\n}\r\n\r\n@media screen and (min-width: 768px) {\r\n    .brain9d-page .image {\r\n        float: right;\r\n    }\r\n}\r\n@media screen and (max-width: 1199px) {\r\n    .brain9d-page .page-note > .note {\r\n        padding-left: 83px;\r\n    }\r\n    .brain9d-page .page-note > .note:before {\r\n        width: 65px;\r\n        height: 93px;\r\n    }\r\n}\r\n@media screen and (max-width: 991px) {\r\n    .faq-heading {\r\n        padding-bottom: 0;\r\n    }\r\n    .sub-heading {\r\n        font-size: 18px;\r\n    }\r\n    .brain9d-page p {\r\n        font-size: 13px;\r\n        margin-bottom: 10px;\r\n    }\r\n    .brain9d-page .desc ul {\r\n        padding-left: 15px;\r\n        padding-right: 15px;\r\n    }\r\n    .brain9d-page .desc ul li {\r\n        font-size: 14px;\r\n    }\r\n    .brain9d-page .desc ul li > i {\r\n        left: -15px;\r\n        top: 6px;\r\n    }\r\n    .brain9d-page .desc .point {\r\n        padding: 10px;\r\n        margin-top: 10px;\r\n        font-size: 12px;\r\n        min-width: 525px;\r\n    }\r\n    .brain9d-page .page-note {\r\n        font-size: 15px;\r\n        margin-top: 10px;\r\n    }\r\n    .brain9d-page .page-note > .note {\r\n        padding-left: 70px;\r\n    }\r\n    .brain9d-page .page-note > .note:before {\r\n        width: 60px;\r\n        height: 84px;\r\n    }\r\n}\r\n@media screen and (max-width: 767px) {\r\n    .brain9d-page .image > img {\r\n        margin: 0 auto;\r\n    }\r\n    .brain9d-page .page-note > .note:before {\r\n        bottom: auto;\r\n        height: 84px;\r\n    }\r\n    .brain9d-page .desc .point {\r\n        min-width: auto;\r\n    }\r\n}\r\n@media screen and (max-width: 650px) {\r\n    .brain9d-page .page-note > .note:before {\r\n        top: 25px;\r\n        height: 84px;\r\n    }\r\n}\r\n@media screen and (max-width: 480px) {\r\n    .brain9d-page p {\r\n        font-size: 16px;\r\n    }\r\n    .brain9d-page .page-note > .note {\r\n        font-size: 12px;\r\n        padding-left: 58px;\r\n    }\r\n    .brain9d-page .page-note > .note:before {\r\n        top: 8px;\r\n        height: 67px;\r\n        width: 47px;\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/brain9d/brain9d.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container brain9d-page\">\r\n    <h3 class=\"faq-heading\" translate=\"brain9dpage.heading\"></h3>\r\n    <h4 class=\"sub-heading text-center\" translate=\"brain9dpage.subheading\"></h4>\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-5 image\">\r\n            <img class=\"img-responsive hidden-xs\" src=\"../../assets/images/brain9d/brain.png\" alt=\"What is brain9d?\" />\r\n            <img class=\"img-responsive visible-xs\" src=\"../../assets/images/brain9d/brain_detail_mobile.png\" alt=\"What is brain9d?\" />\r\n        </div>\r\n        <div class=\"col-sm-7 desc\">\r\n            <p translate=\"brain9dpage.para1\"></p>\r\n            <p translate=\"brain9dpage.para2\"></p>\r\n            <ul>\r\n                <li><i class=\"fa fa-check-circle\"></i><span translate=\"brain9dpage.para2p1\"></span></li>\r\n                <li><i class=\"fa fa-check-circle\"></i><span translate=\"brain9dpage.para2p2\"></span></li>\r\n                <li><i class=\"fa fa-check-circle\"></i><span translate=\"brain9dpage.para2p3\"></span></li>\r\n            </ul>\r\n            <p class=\"point\" translate=\"brain9dpage.point\"></p>\r\n            <br>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <p translate=\"brain9dpage.para3\"></p>\r\n            <p translate=\"brain9dpage.para4\"></p>\r\n            <p class=\"button-container text-center\">\r\n                <a class=\"btn custom-small-btn btn-primary hvr-float-shadow\" [href]=\"config.appDomain+'signup'\" target=\"_blank\" translate=\"buttons.trybrain9dnow\"></a>\r\n            </p>\r\n            <p class=\"special-note\" translate=\"brain9dpage.para5\"></p>\r\n        </div>\r\n        <div class=\"col-sm-12\">\r\n            <div class=\"page-note row\">\r\n                <!-- <div class=\"note\" translate=\"brain9dpage.note\"></div> -->\r\n                <div class=\"img\">\r\n                    <img class=\"\" src=\"../../assets/images/brain9d/note.png\" alt=\"Note\"/>                    \r\n                </div>\r\n                <div class=\"text\">\r\n                    <p translate=\"brain9dpage.note\"></p>                    \r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/brain9d/brain9d.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Brain9dComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Brain9dComponent = (function () {
    function Brain9dComponent(config) {
        this.config = config;
    }
    Brain9dComponent.prototype.ngOnInit = function () {
    };
    return Brain9dComponent;
}());
Brain9dComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-brain9d',
        template: __webpack_require__("../../../../../src/app/brain9d/brain9d.component.html"),
        styles: [__webpack_require__("../../../../../src/app/brain9d/brain9d.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], Brain9dComponent);

var _a;
//# sourceMappingURL=brain9d.component.js.map

/***/ }),

/***/ "../../../../../src/app/config.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfigService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* enableProdMode */])();
var ConfigService = (function () {
    function ConfigService(http) {
        this.http = http;
        this.showLoader = false;
        this.server = "brain9d";
        this.apiDomain = "https://dev.brain9d.com/";
        this.appDomain = "https://api.app.dev.brain9d.com/v1";
        this.facebookAppId = "216915902095824";
        this.forumUrl = "https://app.dev.brain9d.com/forum/index.php";
        this.androidAppUrl = "https://play.google.com/store/apps/details?id=com.brain9d.brainhealth";
        this.iphoneAppUrl = "https://itunes.apple.com/us/app/brain9d/id1176918637?mt=8";
        this.externalLinksUrl = "https://brain9d.com/";
        this.twoCheckoutSid = '103041325';
        this.pushNotificationTopic = 'brain9d';
        this.googleAppId = "990135941036-v886ogicnltqaj6hssssk50vls7v0lcb.apps.googleusercontent.com";
        this.isNativeApp = navigator.userAgent.match(/B9DNativeApp/i); //Detecting Application
        this.readCookie = function (name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                    c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0)
                    return c.substring(nameEQ.length, c.length);
            }
            return null;
        };
        this.createCookie = function (name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toUTCString();
            }
            else
                var expires = "";
            document.cookie = name + "=" + value + expires + "; path=/";
        };
        this.eraseCookie = function (name) {
            this.createCookie(name, "", -1);
        };
        if (window.location.origin.indexOf('dev.') > -1 || window.location.origin.indexOf('localhost') > -1)
            this.server = "development";
        else if (window.location.origin.indexOf('test.') > -1)
            this.server = "test";
        if (this.server === 'development') {
            this.apiDomain = "https://api.app.dev.brain9d.com/v1";
            this.appDomain = "https://app.dev.brain9d.com/";
            this.facebookAppId = "217392898670282";
            this.forumUrl = "https://dev.brain9d.com/forum/index.php";
            this.twoCheckoutSid = '901341537';
            this.pushNotificationTopic = 'brain9d_dev';
        }
        if (this.server === 'test') {
            this.apiDomain = "https://api.test.brain9d.com/v1";
            this.appDomain = "https://app.test.brain9d.com/";
            this.facebookAppId = "487834468280591";
            this.forumUrl = "https://test.brain9d.com/forum/index.php";
            this.twoCheckoutSid = '901341537';
            this.pushNotificationTopic = 'brain9d_dev';
        }
        if (window.location.origin.indexOf('localhost') > -1)
            this.facebookAppId = "370978729918129";
    }
    ConfigService.prototype.deferYoutubeVideo = function () {
        var vidDefer = document.getElementsByTagName('iframe');
        for (var i = 0; i < vidDefer.length; i++) {
            if (vidDefer[i].getAttribute('data-src')) {
                vidDefer[i].setAttribute('src', vidDefer[i].getAttribute('data-src'));
            }
        }
    };
    ConfigService.prototype.getLanguage = function () {
        return this.http.get(this.apiDomain + '/language?is_enable=true')
            .map(function (response) {
            var responseObj = {
                data: response.json(),
                headers: response.headers.toJSON(),
                status: response.status
            };
            return responseObj;
        })
            .toPromise()
            .catch(function (error) {
            var errorObj = {
                data: error.json(),
                status: error.status,
                statusText: error.statusText,
                url: error.url
            };
            throw (errorObj);
        });
    };
    return ConfigService;
}());
ConfigService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object])
], ConfigService);

var _a;
//# sourceMappingURL=config.service.js.map

/***/ }),

/***/ "../../../../../src/app/contactus/contactus.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#contactus .input-group {\r\n    margin-bottom: 25px;\r\n}\r\n#contactus span.error {\r\n    display: block;\r\n    margin-top: -25px;\r\n    width: 100%;\r\n    text-align: left;\r\n    font-size: 12px;\r\n    color: red;\r\n    margin-bottom: 10px;\r\n}\r\n.address-wrapper {\r\n    margin-top:50px;\r\n}\r\n.address-container {\r\n    padding: 10px 10px 20px 10px;\r\n    background: #eee;\r\n    margin-bottom: 30px;\r\n}\r\n.address-container h4 {\r\n    font-size: 20px;\r\n    font-weight: 600;\r\n    color: #00acb8;\r\n}\r\n.address-container p {\r\n    max-width: 250px;\r\n    margin: 0 auto;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/contactus/contactus.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- main body section -->\r\n<div class=\"container\">\r\n    <p style=\"height: 41px;\"> </p>\r\n    <div class=\"form-outline col-md-8 col-sm-12 col-md-offset-2\">\r\n\r\n        <h3 translate=\"contactuspage.heading\"></h3>\r\n        <p class=\"sub-dis\">\r\n            <span translate=\"contactuspage.line1\"></span>\r\n            <br>\r\n            <span translate=\"contactuspage.line2\"></span>\r\n            <br>\r\n            <span translate=\"contactuspage.line3\"></span>\r\n            <a style=\"color:#00ACB8;\" href=\"mailto:info@brain9d.com\">info@brain9d.com</a>\r\n        </p>\r\n        <div class=\"clearfix\"></div>\r\n\r\n\r\n        <h4 class=\"send-message\"><span translate=\"contactuspage.sendusamessage\"></span></h4>\r\n        <form id=\"contactus\" name=\"contactus\" (ngSubmit)=\"submitForm()\" #contactForm=\"ngForm\">\r\n            <div class=\"col-md-4 half-space\">\r\n                <div class=\"input-group\">\r\n                    <input \r\n                        name=\"username\" \r\n                        placeholder=\"{{ 'contactuspage.enterfullname' | translate }}\" \r\n                        class=\"form-control\" \r\n                        id=\"username\" \r\n                        ngModel \r\n                        required\r\n                        #username=\"ngModel\"\r\n                    >\r\n                </div>\r\n                <span class=\"error\" *ngIf=\"(contactForm.submitted || username.touched) && username.invalid\" translate=\"validationmessages.required\"></span>\r\n            </div>\r\n            <div class=\"col-md-4 half-space\">\r\n                <div class=\"input-group\">\r\n                    <input \r\n                        name=\"email\" \r\n                        type=\"email\" \r\n                        placeholder=\"{{ 'contactuspage.enteremail' | translate }}\" \r\n                        class=\"form-control\" \r\n                        id=\"email\" \r\n                        ngModel \r\n                        required \r\n                        email\r\n                        #email=\"ngModel\"\r\n                    >\r\n                </div>\r\n                <span class=\"error\" *ngIf=\"(contactForm.submitted || email.touched) && email.errors && email.errors.required\" translate=\"validationmessages.required\"></span>\r\n                <span class=\"error\" *ngIf=\"(contactForm.submitted || email.touched) && email.errors && email.errors.email && !email.errors.required\" translate=\"validationmessages.email\"></span>\r\n            </div>\r\n            <div class=\"col-md-4 half-space last-item\">\r\n                <div class=\"input-group \">\r\n                    <input \r\n                        id=\"subject\" \r\n                        class=\"form-control\" \r\n                        name=\"subject\" \r\n                        placeholder=\"{{ 'contactuspage.entersubject' | translate }}\" \r\n                        ngModel \r\n                        required\r\n                        #subject=\"ngModel\"\r\n                    >\r\n                </div>\r\n                <span class=\"error\" *ngIf=\"(contactForm.submitted || subject.touched) && subject.errors && subject.errors.required\" translate=\"validationmessages.required\"></span>\r\n            </div>\r\n            <div class=\"input-group\">\r\n                <textarea \r\n                    class=\"Whiteboard\" \r\n                    rows=\"6\" \r\n                    name=\"message\" \r\n                    placeholder=\"{{ 'contactuspage.message' | translate }}\" \r\n                    ngModel \r\n                    required\r\n                    #message=\"ngModel\"\r\n                ></textarea>\r\n            </div>\r\n            <span class=\"error\" *ngIf=\"(contactForm.submitted || message.touched) && message.errors && message.errors.required\" translate=\"validationmessages.required\"></span>\r\n\r\n            <p style=\"height: 10px;\"> </p>\r\n            <p *ngIf=\"submittedSuccessfully == false \" class=\"alert-danger\" translate=\"messages.errorsendingmessage\"></p>\r\n            <p *ngIf=\"submittedSuccessfully ==  true \" class=\"alert-success\" translate=\"messages.messagesent\"></p>\r\n            <button class=\"btn custom-small-btn btn-primary hvr-float-shadow\" role=\"button\" type=\"submit\" style=\"width:171px\" translate=\"buttons.sendmessage\"></button>\r\n        </form>\r\n        \r\n        <div class=\"address-wrapper\">\r\n            <!-- <h3 translate=\"contactuspage.ouroffices\"></h3> -->\r\n            <div class=\"address-container\" *ngFor=\"let address of addresses\">\r\n                <h4>{{ address.cd_heading }}</h4>\r\n                <p>{{ address.cd_address }}</p>\r\n                <a href=\"mailto:{{address.cd_email}}\">{{ address.cd_email }}</a><br>\r\n                <p>{{ address.cd_phone }}</p>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- main body section -->"

/***/ }),

/***/ "../../../../../src/app/contactus/contactus.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactusComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactusComponent = (function () {
    function ContactusComponent(config, http) {
        this.config = config;
        this.http = http;
        this.addresses = [];
        this.getAddresses();
    }
    ContactusComponent.prototype.ngOnInit = function () {
    };
    ContactusComponent.prototype.submitForm = function () {
        var _this = this;
        this.firstInvalid = document.getElementsByClassName("ng-invalid")[1];
        if (this.contactForm.invalid) {
            this.firstInvalid.focus();
        }
        else {
            this.config.showLoader = true;
            this.http.post(this.config.apiDomain + '/users/contactus', {
                "name": this.contactForm.value.username,
                "email": this.contactForm.value.email,
                "subject": this.contactForm.value.subject,
                "message": this.contactForm.value.message,
                "lang_code": "en-USS"
            })
                .subscribe(function (response) {
                if (response) {
                    _this.config.showLoader = false;
                    _this.contactForm.resetForm();
                    _this.submittedSuccessfully = true;
                }
            }, function (error) {
                _this.config.showLoader = false;
                _this.submittedSuccessfully = false;
            });
        }
    };
    ContactusComponent.prototype.getAddresses = function () {
        var _this = this;
        this.config.showLoader = true;
        this.http.get(this.config.apiDomain + '/contact-detail?visible=1')
            .subscribe(function (response) {
            var headers = response.headers;
            _this.config.showLoader = false;
            if (response) {
                _this.addresses = response.json();
            }
        }, function (error) {
            _this.config.showLoader = false;
        });
    };
    ;
    return ContactusComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_16" /* ViewChild */])('contactForm'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* NgForm */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* NgForm */]) === "function" && _a || Object)
], ContactusComponent.prototype, "contactForm", void 0);
ContactusComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-contactus',
        template: __webpack_require__("../../../../../src/app/contactus/contactus.component.html"),
        styles: [__webpack_require__("../../../../../src/app/contactus/contactus.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__config_service__["a" /* ConfigService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _c || Object])
], ContactusComponent);

var _a, _b, _c;
//# sourceMappingURL=contactus.component.js.map

/***/ }),

/***/ "../../../../../src/app/disclaimer-copyrights/disclaimer-copyrights.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/disclaimer-copyrights/disclaimer-copyrights.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- main body section -->\r\n<div class=\"main-content-area container\">\r\n    <h3 class=\"faq-heading\" translate=\"disclaimerandcopyrightspage.heading\"></h3>\r\n    <p translate=\"disclaimerandcopyrightspage.firstpara\"></p>\r\n    <p>\r\n        <span translate=\"disclaimerandcopyrightspage.secondpara1\"></span>\r\n        <a href=\"mailto:ip@brain9d.com\">ip@brain9d.com</a>\r\n        <span translate=\"disclaimerandcopyrightspage.secondpara2\"></span>\r\n    </p>\r\n    <p id=\"year\"><span translate=\"headerfooter.footercopyrights\"></span> &copy; <span class=\"year\"></span> </p>\r\n\r\n</div>\r\n<script>\r\n    var d = new Date();\r\n    $(\"#year span.year\").text(d.getFullYear());\r\n</script>"

/***/ }),

/***/ "../../../../../src/app/disclaimer-copyrights/disclaimer-copyrights.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DisclaimerCopyrightsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DisclaimerCopyrightsComponent = (function () {
    function DisclaimerCopyrightsComponent() {
    }
    DisclaimerCopyrightsComponent.prototype.ngOnInit = function () {
    };
    return DisclaimerCopyrightsComponent;
}());
DisclaimerCopyrightsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-disclaimer-copyrights',
        template: __webpack_require__("../../../../../src/app/disclaimer-copyrights/disclaimer-copyrights.component.html"),
        styles: [__webpack_require__("../../../../../src/app/disclaimer-copyrights/disclaimer-copyrights.component.css")]
    }),
    __metadata("design:paramtypes", [])
], DisclaimerCopyrightsComponent);

//# sourceMappingURL=disclaimer-copyrights.component.js.map

/***/ }),

/***/ "../../../../../src/app/employment/employment.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/employment/employment.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <h3 class=\"faq-heading\" translate=\"employmentpage.heading\"></h3>\r\n  <p translate=\"employmentpage.firstpara\"></p>\r\n  <p style=\"font-weight:bold\" translate=\"employmentpage.subheading\"></p>\r\n  <p translate=\"employmentpage.listtitle\"></p>\r\n  <ul class=\"list-disc\">\r\n    <li translate=\"employmentpage.point1\"></li>\r\n    <li translate=\"employmentpage.point2\"></li>\r\n    <li translate=\"employmentpage.point3\"></li>\r\n    <li translate=\"employmentpage.point4\"></li>\r\n  </ul>\r\n  <p class=\"note-content\">\r\n    <span translate=\"employmentpage.calltoaction\"></span>\r\n    <a href=\"mailto:jobs@brain9d.com\">jobs@brain9d.com</a>\r\n  </p>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/employment/employment.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmploymentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EmploymentComponent = (function () {
    function EmploymentComponent() {
    }
    EmploymentComponent.prototype.ngOnInit = function () {
    };
    return EmploymentComponent;
}());
EmploymentComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-employment',
        template: __webpack_require__("../../../../../src/app/employment/employment.component.html"),
        styles: [__webpack_require__("../../../../../src/app/employment/employment.component.css")]
    }),
    __metadata("design:paramtypes", [])
], EmploymentComponent);

//# sourceMappingURL=employment.component.js.map

/***/ }),

/***/ "../../../../../src/app/faq/faq.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/faq/faq.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- main body section -->\r\n\r\n<h3 class=\"faq-heading\" translate=\"faqpage.heading\"></h3>\r\n<div class=\"panel-group container\" id=\"accordion'\">\r\n\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading \">\r\n            <h4 class=\"panel-title\">\r\n                <a role=\"button\" [attr.data-toggle]=\"'collapse'\" class=\"collapsed\" [attr.data-parent]=\"'#accordion'\" href=\"#collapse1\" translate=\"faqpage.question1\"></a>\r\n            </h4>\r\n        </div>\r\n        <div id=\"collapse1\" class=\"panel-collapse collapse \">\r\n            <div>\r\n                <div class=\"panel-body\" translate=\"faqpage.answer1\"></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading \">\r\n            <h4 class=\"panel-title\">\r\n                <a role=\"button\" [attr.data-toggle]=\"'collapse'\" class=\"collapsed\" [attr.data-parent]=\"'#accordion'\" href=\"#collapse2\" translate=\"faqpage.question2\"></a>\r\n            </h4>\r\n        </div>\r\n        <div id=\"collapse2\" class=\"panel-collapse collapse \">\r\n            <div>\r\n                <div class=\"panel-body\" translate=\"faqpage.answer2\"></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading \">\r\n            <h4 class=\"panel-title\">\r\n                <a role=\"button\" [attr.data-toggle]=\"'collapse'\" class=\"collapsed\" [attr.data-parent]=\"'#accordion'\" href=\"#collapse3\" translate=\"faqpage.question3\"></a>\r\n            </h4>\r\n        </div>\r\n        <div id=\"collapse3\" class=\"panel-collapse collapse \">\r\n            <div>\r\n                <div class=\"panel-body\" translate=\"faqpage.answer3\"></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading \">\r\n            <h4 class=\"panel-title\">\r\n                <a role=\"button\" [attr.data-toggle]=\"'collapse'\" class=\"collapsed\" [attr.data-parent]=\"'#accordion'\" href=\"#collapse4\" translate=\"faqpage.question4\"></a>\r\n            </h4>\r\n        </div>\r\n        <div id=\"collapse4\" class=\"panel-collapse collapse \">\r\n            <div>\r\n                <div class=\"panel-body\">\r\n                    <span translate=\"faqpage.answer4-1\"></span>\r\n                    <br>\r\n                    <span translate=\"faqpage.answer4-2\"></span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading \">\r\n            <h4 class=\"panel-title\">\r\n                <a role=\"button\" [attr.data-toggle]=\"'collapse'\" class=\"collapsed\" [attr.data-parent]=\"'#accordion'\" href=\"#collapse5\" translate=\"faqpage.question5\"></a>\r\n            </h4>\r\n        </div>\r\n        <div id=\"collapse5\" class=\"panel-collapse collapse \">\r\n            <div>\r\n                <div class=\"panel-body\" translate=\"faqpage.answer5\"></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading \">\r\n            <h4 class=\"panel-title\">\r\n                <a role=\"button\" [attr.data-toggle]=\"'collapse'\" class=\"collapsed\" [attr.data-parent]=\"'#accordion'\" href=\"#collapse6\" translate=\"faqpage.question6\"></a>\r\n            </h4>\r\n        </div>\r\n        <div id=\"collapse6\" class=\"panel-collapse collapse \">\r\n            <div>\r\n                <div class=\"panel-body\" translate=\"faqpage.answer6\"></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"panel panel-default\">\r\n        <div class=\"panel-heading \">\r\n            <h4 class=\"panel-title\">\r\n                <a role=\"button\" [attr.data-toggle]=\"'collapse'\" class=\"collapsed\" [attr.data-parent]=\"'#accordion'\" href=\"#collapse7\" translate=\"faqpage.question7\"></a>\r\n            </h4>\r\n        </div>\r\n        <div id=\"collapse7\" class=\"panel-collapse collapse \">\r\n            <div>\r\n                <div class=\"panel-body\" translate=\"faqpage.answer7\"></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- main body section -->"

/***/ }),

/***/ "../../../../../src/app/faq/faq.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FaqComponent = (function () {
    function FaqComponent() {
    }
    FaqComponent.prototype.ngOnInit = function () {
    };
    return FaqComponent;
}());
FaqComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-faq',
        template: __webpack_require__("../../../../../src/app/faq/faq.component.html"),
        styles: [__webpack_require__("../../../../../src/app/faq/faq.component.css")]
    }),
    __metadata("design:paramtypes", [])
], FaqComponent);

//# sourceMappingURL=faq.component.js.map

/***/ }),

/***/ "../../../../../src/app/feedback-listing/feedback-listing.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/feedback-listing/feedback-listing.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- main body section -->\r\n<div class=\"main-area\">\r\n    <h3 class=\"faq-heading\" translate=\"homepage.testimonialsheading\"></h3>\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div *ngFor=\"let fb of feedbacks\" class=\"col-sm-12\">\r\n                <div id=\"tb-testimonial\" class=\"testimonial testimonial-primary\">\r\n                    <div class=\"testimonial-desc\">\r\n                        <img [src]=\"fb.fbk_image\">\r\n                        <div class=\"testimonial-writer\">\r\n                            <div class=\"testimonial-writer-name\">{{fb.feedbackUser.usr_first_name}} {{fb.feedbackUser.usr_last_name}}</div>\r\n                            <div class=\"testimonial-writer-designation\"> {{fb.fbk_created_at.replace(\" \", \"T\")+\".000Z\" | date : \"medium\"}} </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"testimonial-section\">\r\n                        <span *ngIf=\"fb.fbk_feedback_message\">\"{{fb.fbk_feedback_message}}\"</span>\r\n                        <p class=\"ratings\">\r\n                            <i class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                            <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 2}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                            <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 3}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                            <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 4}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                            <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 5}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                        </p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <ul class=\"pagination pull-right\">\r\n            <li [ngClass]=\"{'disabled': currentPage==1}\">\r\n                <a (click)=\"getFeedback(currentPage-1)\">&laquo;</a>\r\n            </li>\r\n            <li [ngClass]=\"{'active':page.isCurrent}\" *ngFor=\"let page of pagination\">\r\n                <a (click)=\"getFeedback(page.page_num)\">{{page.page_num}}</a>\r\n            </li>\r\n            <li [ngClass]=\"{'disabled': currentPage==totalPages}\">\r\n                <a (click)=\"getFeedback(currentPage+1)\">&raquo;</a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n<!-- main body section -->"

/***/ }),

/***/ "../../../../../src/app/feedback-listing/feedback-listing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedbackListingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FeedbackListingComponent = (function () {
    function FeedbackListingComponent(http, config) {
        this.http = http;
        this.config = config;
        this.pagination = [];
        this.feedbacks = [];
        this.newPage = {
            page_num: 0,
            isCurrent: false
        };
    }
    FeedbackListingComponent.prototype.ngOnInit = function () {
        this.getFeedback(1);
    };
    FeedbackListingComponent.prototype.getFeedback = function (pageNumber) {
        var _this = this;
        this.config.showLoader = true;
        if (pageNumber == 0 || pageNumber == this.currentPage || pageNumber == this.totalPages + 1)
            return;
        this.pagination = [];
        this.query = "?ipp=10&&fbk_status=publish&&expand=feedbackUser";
        if (pageNumber)
            this.query += '&page=' + pageNumber;
        this.http.get(this.config.apiDomain + '/feedback' + this.query)
            .subscribe(function (response) {
            var headers = response.headers;
            _this.config.showLoader = false;
            if (response) {
                var domain_1 = _this.config.apiDomain.replace("v1", "uploads/");
                _this.feedbacks = response.json().map(function (feedback) {
                    if (feedback.feedbackUser && feedback.feedbackUser.usr_profile_picture)
                        feedback.fbk_image = domain_1 + feedback.feedbackUser.usr_profile_picture;
                    else
                        feedback.fbk_image = "../assets/images/feed_back.png";
                    return feedback;
                });
                _this.currentPage = parseInt(headers.get('X-Pagination-Current-Page'));
                _this.totalPages = parseInt(headers.get('X-Pagination-Page-Count'));
                var recordsPerPage = parseInt(headers.get('X-Pagination-Per-Page'));
                _this.totalRecords = parseInt(headers.get('X-Pagination-Total-Count'));
                for (var i = 1; i <= _this.totalPages; i++) {
                    _this.newPage = {
                        page_num: 0,
                        isCurrent: false
                    };
                    _this.newPage.page_num = i;
                    _this.newPage.isCurrent = _this.currentPage == i ? true : false;
                    _this.pagination.push(_this.newPage);
                }
            }
        }, function (error) {
            _this.config.showLoader = false;
        });
    };
    ;
    return FeedbackListingComponent;
}());
FeedbackListingComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-feedback-listing',
        template: __webpack_require__("../../../../../src/app/feedback-listing/feedback-listing.component.html"),
        styles: [__webpack_require__("../../../../../src/app/feedback-listing/feedback-listing.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* ConfigService */]) === "function" && _b || Object])
], FeedbackListingComponent);

var _a, _b;
//# sourceMappingURL=feedback-listing.component.js.map

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer>\r\n    <div class=\"container\">\r\n        <div class=\"col-md-4 col-sm-4\">\r\n            <ul>\r\n                <h3 translate=\"headerfooter.quicklinks\"></h3>\r\n                <li>\r\n                    <a routerLink=\"/brain9d\" translate=\"headerfooter.brain9d\"></a>\r\n                </li>\r\n                <li>\r\n                    <a routerLink=\"/how-it-works\" translate=\"headerfooter.howitworks\"></a>\r\n                </li>\r\n                <li>\r\n                    <a [href]=\"config.appDomain+'demograph'\" target=\"_blank\" translate=\"headerfooter.demograph\"></a>\r\n                </li>\r\n                <li>\r\n                    <a routerLink=\"/employment\" translate=\"headerfooter.careers\"></a>\r\n                </li>\r\n                <li>\r\n                    <a routerLink=\"/contact-us\" translate=\"headerfooter.contactus\"></a>\r\n                </li>\r\n                <li>\r\n                    <a routerLink=\"/copyrights\" translate=\"headerfooter.copyrights\"></a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n        <div class=\"col-md-4 col-sm-4 members-links\">\r\n            <ul>\r\n                <h3 translate=\"headerfooter.memberlinks\"></h3>\r\n                <li>\r\n                    <a [href]=\"config.appDomain + 'dashboard'\" target=\"_blank\" translate=\"headerfooter.memberworkspace\"></a>\r\n                </li>\r\n                <li>\r\n                    <a [href]=\"config.appDomain + 'brain-and-mental'\" target=\"_blank\" translate=\"headerfooter.brainandmental\"></a>\r\n                </li>\r\n                <li>\r\n                    <a [href]=\"config.appDomain + 'mood-and-behavioral'\" target=\"_blank\" translate=\"headerfooter.cognitiveandbehavioural\"></a>\r\n                </li>\r\n                <li>\r\n                    <a routerLink=\"/faq\" translate=\"headerfooter.faq\"></a>\r\n                </li>\r\n                <li>\r\n                    <a routerLink=\"/terms-and-conditions\" translate=\"headerfooter.termsandconditions\"></a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n        <div class=\"col-md-4 social-col col-sm-4\">\r\n            <h3 translate=\"headerfooter.followus\"></h3>\r\n            <p>\r\n                <a href=\"https://www.facebook.com/brain9dbrainfitness/\" target=\"_blank\"><img src=\"../assets/images/footer-icons/facebook.png\"></a>\r\n                <!-- <a href=\"https://twitter.com/search?q=brain9d\" target=\"_blank\"><img src=\"../assets/images/footer-icons/twitter.png\"></a> -->\r\n                <a href=\"https://plus.google.com/u/0/b/106617861268642769811/106617861268642769811\" target=\"_blank\"><img src=\"../assets/images/footer-icons/google.png\"></a>\r\n                <a href=\"https://www.youtube.com/channel/UC431O0UwhAOELyItx4rBFWQ?spfreload=10\" target=\"_blank\"><img src=\"../assets/images/footer-icons/youtube.png\"></a>\r\n            </p>\r\n            <p>\r\n                <!-- <a href=\"https://my.linkedin.com/in/brain-nan-208b2a12a\" target=\"_blank\"><img src=\"../assets/images/footer-icons/linkdin.png\"></a> -->\r\n                <!-- <a href=\"https://www.pinterest.com/nanmalik/brain9d/\" target=\"_blank\"><img src=\"../assets/images/footer-icons/pinterest.png\"></a> -->\r\n                <!-- <a href=\"https://www.youtube.com/channel/UC431O0UwhAOELyItx4rBFWQ?spfreload=10\" target=\"_blank\"><img src=\"../assets/images/footer-icons/youtube.png\"></a> -->\r\n            </p>\r\n        </div>\r\n    </div>\r\n    <div class=\"copyrights\">\r\n        <p id=\"year\"><span translate=\"headerfooter.footercopyrights\"></span> &copy; <span class=\"year\">{{ copyrightDate }}</span>\r\n        </p>\r\n    </div>\r\n</footer>"

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FooterComponent = (function () {
    function FooterComponent(config) {
        this.config = config;
        var d = new Date();
        this.copyrightDate = d.getFullYear();
    }
    FooterComponent.prototype.ngOnInit = function () { };
    return FooterComponent;
}());
FooterComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["o" /* Component */])({
        selector: 'app-footer',
        template: __webpack_require__("../../../../../src/app/footer/footer.component.html"),
        styles: [__webpack_require__("../../../../../src/app/footer/footer.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], FooterComponent);

var _a;
//# sourceMappingURL=footer.component.js.map

/***/ }),

/***/ "../../../../../src/app/get-language.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GetLanguageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GetLanguageService = (function () {
    function GetLanguageService(http, config) {
        this.http = http;
        this.config = config;
        this.getLanguage();
    }
    GetLanguageService.prototype.getLanguage = function () {
        return this.http.get(this.config.apiDomain + '/language?is_enable=true')
            .map(function (response) {
            var responseObj = {
                data: response.json(),
                headers: response.headers.toJSON(),
                status: response.status
            };
            return responseObj;
        })
            .catch(function (error) {
            var errorObj = {
                data: error.json(),
                status: error.status,
                statusText: error.statusText,
                url: error.url
            };
            throw (errorObj);
        });
    };
    return GetLanguageService;
}());
GetLanguageService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* ConfigService */]) === "function" && _b || Object])
], GetLanguageService);

var _a, _b;
//# sourceMappingURL=get-language.service.js.map

/***/ }),

/***/ "../../../../../src/app/header/header.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Header -->\r\n<div class=\"navbar-wrapper\" id=\"main-navigation\" *ngIf=\"!config.isNativeApp\">\r\n    <div class=\"container\">\r\n        <nav class=\"navbar navbar-inverse navbar-static-top\">\r\n            <div class=\"\">\r\n                <div class=\"navbar-header\">\r\n                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">\r\n                    <span class=\"sr-only\">Toggle navigation</span>\r\n                    <span class=\"icon-bar\"></span>\r\n                    <span class=\"icon-bar\"></span>\r\n                    <span class=\"icon-bar\"></span>\r\n                    </button>\r\n                    <a class=\"navbar-brand home-logo\" routerLink=\"/\"><img src=\"../../assets/images/logo-home.png\"></a>\r\n                    <a class=\"navbar-brand main-logo\" routerLink=\"/\"><img src=\"../../assets/images/logo-home.png\"></a>\r\n                </div>\r\n                <div id=\"navbar\" class=\"navbar-collapse collapse\">\r\n                    <ul class=\"nav navbar-nav\">\r\n                        <li>\r\n                            <a routerLink=\"/\" translate=\"headerfooter.home\"></a>\r\n                        </li>\r\n                        <li class=\"dropdown dropMenu\">\r\n                            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"><span translate=\"headerfooter.brain9d\"></span> <span class=\"caret-dw\"></span></a>\r\n                            <ul class=\"dropdown-menu first-dd\">\r\n                                <li>\r\n                                    <a routerLink=\"/brain9d\" translate=\"headerfooter.whatisbrain9d\"></a>\r\n                                </li>\r\n                                <li>\r\n                                    <a routerLink=\"/how-it-works\" translate=\"headerfooter.howitworks\"></a>\r\n                                </li>\r\n                                <li>\r\n                                    <a routerLink=\"/membership\" translate=\"headerfooter.membership\"></a>\r\n                                </li>\r\n                                <li>\r\n                                    <a routerLink=\"/testimonials\" translate=\"headerfooter.testimonials\"></a>\r\n                                </li>\r\n                                <li>\r\n                                    <a routerLink=\"/contact-us\" translate=\"headerfooter.contactus\"></a>\r\n                                </li>\r\n                            </ul>\r\n                        </li>\r\n\r\n                        <li class=\"dropdown\">\r\n                            <a routerLink=\"/brain-health\" translate=\"headerfooter.brainhealth\"></a>\r\n                        </li>\r\n                        <li class=\"dropdown\">\r\n                            <a routerLink=\"/learning-center\" translate=\"headerfooter.education\"></a>\r\n                        </li>\r\n                        <li class=\"dropdown\">\r\n                            <a routerLink=\"/blog\" translate=\"headerfooter.blog\"></a>\r\n                        </li>\r\n                        <li class=\"dropdown\">\r\n                            <a routerLink=\"/brain-shop\" translate=\"headerfooter.brainshop\"></a>\r\n                        </li>\r\n\r\n                        <li class=\"dropdown useraccount languages dropMenu\">\r\n                            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"> <span class=\"flag flag-{{ config.currentLanguage?.lang_code }}\"></span><span [innerHtml]=\"config.currentLanguage?.lang_label\"></span> <span class=\"caret-dw\"></span></a>\r\n                            <ul class=\"dropdown-menu last-dd\">\r\n                                <li *ngFor=\"let language of config.languages\">\r\n                                    <a (click)=\"applyLanguageChange(language.lang_code)\">\r\n                                        <span class=\"flag flag-{{ language.lang_code}}\"></span>{{ language.lang_label }}\r\n                                    </a>\r\n                                </li>\r\n                            </ul>\r\n                        </li>\r\n\r\n                        <li class=\"login\"><a [href]=\"config.appDomain+'login'\" target=\"_blank\"><i class=\"fa fa-user\" aria-hidden=\"true\"></i> <span translate=\"headerfooter.login\"></span></a></li>\r\n                    </ul>\r\n                </div>\r\n            </div>\r\n        </nav>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HeaderComponent = (function () {
    function HeaderComponent(router, config) {
        var _this = this;
        this.router = router;
        this.config = config;
        this.changeLanguage = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* EventEmitter */]();
        this.isHome = false;
        router.events.forEach(function (event) {
            if (event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* NavigationEnd */]) {
                if (event.url == '/')
                    _this.isHome = true;
                else
                    _this.isHome = false;
            }
        });
    }
    HeaderComponent.prototype.ngOnInit = function () {
        jQuery("body").on("click", ".dropMenu > a", function () {
            jQuery(this).siblings(".dropdown-menu").toggle();
        });
        jQuery("body").on("click", ".dropMenu .dropdown-menu a", function () {
            jQuery(this).parents(".dropdown-menu").toggle();
        });
    };
    HeaderComponent.prototype.ngAfterViewInit = function () {
        jQuery("#navbar li:not('.dropMenu') a").on("click", function () {
            jQuery("#navbar").collapse("hide");
        });
    };
    HeaderComponent.prototype.applyLanguageChange = function (language) {
        var index = this.config.languages.map(function (e) { return e.lang_code; }).indexOf(language);
        this.changeLanguage.emit(this.config.languages[index]);
        jQuery("#navbar").collapse("hide");
    };
    return HeaderComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Output */])(),
    __metadata("design:type", Object)
], HeaderComponent.prototype, "changeLanguage", void 0);
HeaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-header',
        template: __webpack_require__("../../../../../src/app/header/header.component.html"),
        styles: [__webpack_require__("../../../../../src/app/header/header.component.css")],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["f" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["f" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* ConfigService */]) === "function" && _b || Object])
], HeaderComponent);

var _a, _b;
//# sourceMappingURL=header.component.js.map

/***/ }),

/***/ "../../../../../src/app/home/ads-cta/ads-cta.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".ad-sec {\r\n    background-image: url(" + __webpack_require__("../../../../../src/assets/images/ads-bg.jpg") + ");\r\n    height: 250px;\r\n    background-size: cover;\r\n    background-position: 100% 50%;\r\n    background-color: #f3f3f3;\r\n    margin-bottom: 20px;\r\n}\r\n.ad-sec .content {\r\n    max-width: 550px;\r\n    top: 50%;\r\n    position: absolute;\r\n    -webkit-transform: translateY(-50%);\r\n    transform: translateY(-50%);\r\n    margin-left: 50px;\r\n}\r\n.ad-sec .content h2 {\r\n    margin-top: 0;\r\n    color: #00acb8;\r\n    font-size: 36px;\r\n    font-weight: 500;\r\n    letter-spacing: 1px;\r\n}\r\n.ad-sec .content p {\r\n    font-size: 18px;\r\n}\r\n.post-an-ad {\r\n    position: absolute;\r\n    bottom: 10px;\r\n    right: 10px;\r\n    font-size: 20px;\r\n    letter-spacing: 1px;\r\n    text-transform: uppercase;\r\n    border: solid 2px #fff;\r\n    background: #fff;\r\n    padding: 7px 20px;\r\n    border-radius: 25px;\r\n    text-decoration: none;\r\n    color: #00acb8;\r\n}\r\n.post-an-ad:hover {\r\n    background: transparent;\r\n    color: #fff;\r\n    text-decoration: none;\r\n}\r\n.ad-sec .badge {\r\n    position: absolute;\r\n    padding: 23px 10px;\r\n    height: 150px;\r\n    width: 125px;\r\n    background-size: cover;\r\n    background-position: 50% 100%;\r\n    box-shadow: 0 0 10px #ccc;\r\n    top: 0;\r\n    background-image: url(" + __webpack_require__("../../../../../src/assets/images/ads-badge.png") + ");\r\n    right: 38%;\r\n}\r\n.badge .starting-from {\r\n    width: 100px;\r\n    margin-left: 7px;\r\n    font-weight: 600;\r\n}\r\n.badge .amount {\r\n    font-size: 70px;\r\n    margin-left: 10px;\r\n    width: 100px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/ads-cta/ads-cta.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"ads-cta\" class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 ad-sec\">\r\n      <div class=\"content\">\r\n        <h2 translate=\"adscta.heading\"></h2>\r\n        <p translate=\"adscta.description\"></p>\r\n      </div>\r\n      <div class=\"badge\">\r\n        <div class=\"starting-from\" translate=\"adscta.startingfrom\"></div>\r\n        <div class=\"amount\" translate=\"adscta.amount\"></div>\r\n      </div>\r\n      <a class=\"btn btn-primary post-an-ad\" [href]=\"\" translate=\"buttons.startnow\"></a>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/home/ads-cta/ads-cta.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdsCtaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AdsCtaComponent = (function () {
    function AdsCtaComponent() {
    }
    AdsCtaComponent.prototype.ngOnInit = function () {
    };
    return AdsCtaComponent;
}());
AdsCtaComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-ads-cta',
        template: __webpack_require__("../../../../../src/app/home/ads-cta/ads-cta.component.html"),
        styles: [__webpack_require__("../../../../../src/app/home/ads-cta/ads-cta.component.css")]
    }),
    __metadata("design:paramtypes", [])
], AdsCtaComponent);

//# sourceMappingURL=ads-cta.component.js.map

/***/ }),

/***/ "../../../../../src/app/home/brain9d-section/brain9d-section.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".brain9d {\r\n    padding: 50px 15px;\r\n}\r\n.brain9d .description .section-heading {\r\n    font-size: 36px;\r\n    color: #00acb8;\r\n    margin-bottom: 30px;\r\n}\r\n.brain9d .description p {\r\n    font-size: 16px;\r\n    text-align: justify;\r\n    margin-bottom: 25px;\r\n}\r\n.brain9d .image img {\r\n    margin: 0 auto;\r\n}\r\n\r\n@media screen and (max-width: 1199px) {\r\n    .brain9d .description .section-heading {\r\n        font-size: 32px;\r\n        margin-top: 5px;\r\n        margin-bottom: 15px;\r\n    }\r\n    .brain9d .description p {\r\n        margin-bottom: 12px;\r\n    }\r\n}\r\n@media screen and (max-width: 991px) {\r\n    .brain9d .description .section-heading {\r\n        font-size: 20px;\r\n        margin-top: 0px;\r\n        margin-bottom: 5px;\r\n    }\r\n    .brain9d .description p {\r\n        font-size: 13px;\r\n        margin-bottom: 7px;\r\n    }\r\n    .brain9d button {\r\n        padding: 7px 10px !important;\r\n        margin-top: 8px;\r\n    }\r\n}\r\n@media screen and (max-width: 767px) {\r\n    .brain9d .image img {\r\n        margin-bottom: 25px;\r\n    }\r\n    .brain9d {\r\n        padding: 35px 15px;\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/brain9d-section/brain9d-section.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container brain9d\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-5 col-md-5 image\">\r\n      <img class=\"img-responsive\" src=\"../../../assets/images/home/brain.png\" alt=\"What is brain9d\"/>\r\n    </div>\r\n    <div class=\"col-sm-7 col-md-7 description\">\r\n      <h3 class=\"section-heading\" translate=\"brain9dsection.heading\"></h3>\r\n      <p translate=\"brain9dsection.para1\"></p>\r\n      <p translate=\"brain9dsection.para2\"></p>\r\n      <p translate=\"brain9dsection.para3\"></p>\r\n      <button class=\"btn btn-primary custom-small-btn\" routerLink=\"/brain9d\" translate=\"buttons.learnmore\"></button>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/home/brain9d-section/brain9d-section.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Brain9dSectionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Brain9dSectionComponent = (function () {
    function Brain9dSectionComponent() {
    }
    Brain9dSectionComponent.prototype.ngOnInit = function () {
    };
    return Brain9dSectionComponent;
}());
Brain9dSectionComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-brain9d-section',
        template: __webpack_require__("../../../../../src/app/home/brain9d-section/brain9d-section.component.html"),
        styles: [__webpack_require__("../../../../../src/app/home/brain9d-section/brain9d-section.component.css")]
    }),
    __metadata("design:paramtypes", [])
], Brain9dSectionComponent);

//# sourceMappingURL=brain9d-section.component.js.map

/***/ }),

/***/ "../../../../../src/app/home/get-started/get-started.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".get-started {\r\n    padding: 50px 15px;\r\n}\r\n.get-started .description .section-heading {\r\n    font-size: 36px;\r\n    color: #00acb8;\r\n    margin-bottom: 30px;\r\n}\r\n.get-started .description p {\r\n    font-size: 16px;\r\n    text-align: justify;\r\n    margin-bottom: 25px;\r\n}\r\n.get-started .image img {\r\n    margin: 0 auto;\r\n}\r\n\r\n@media screen and (max-width: 1199px) {\r\n    .get-started .description .section-heading {\r\n        font-size: 32px;\r\n        margin-top: 5px;\r\n        margin-bottom: 5px;\r\n    }\r\n    .get-started .description p {\r\n        margin-bottom: 12px;\r\n    }\r\n    .get-started .image img {\r\n        max-width: 200px;\r\n    }\r\n}\r\n@media screen and (max-width: 991px) {\r\n    .get-started .description .section-heading {\r\n        font-size: 20px;\r\n        margin-top: 0px;\r\n        margin-bottom: 5px;\r\n    }\r\n    .get-started .description p {\r\n        font-size: 13px;\r\n        margin-bottom: 7px;\r\n    }\r\n    .get-started button {\r\n        padding: 7px 10px !important;\r\n        margin-top: 8px;\r\n    }\r\n    .get-started .image img {\r\n        max-width: 175px;\r\n    }\r\n}\r\n@media screen and (max-width: 767px) {\r\n    .get-started .image img {\r\n        margin-bottom: 25px;\r\n    }\r\n    .get-started {\r\n        padding: 35px 15px;\r\n    }\r\n    .get-started .image img {\r\n        max-width: 100%;\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/get-started/get-started.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container get-started\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-5 col-md-5 image\">\r\n      <img class=\"img-responsive\" src=\"../../../assets/images/home/started.png\" alt=\"What is brain9d\" />\r\n    </div>\r\n    <div class=\"col-sm-7 col-md-7 description\">\r\n      <h3 class=\"section-heading\" translate=\"getstartedsection.heading\"></h3>\r\n      <p translate=\"getstartedsection.para1\"></p>\r\n      <p translate=\"getstartedsection.para2\"></p>\r\n      <button class=\"btn btn-primary custom-small-btn\" routerLink=\"/membership\" translate=\"buttons.createfreeaccount\"></button>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/home/get-started/get-started.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GetStartedComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GetStartedComponent = (function () {
    function GetStartedComponent() {
    }
    GetStartedComponent.prototype.ngOnInit = function () {
    };
    return GetStartedComponent;
}());
GetStartedComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-get-started',
        template: __webpack_require__("../../../../../src/app/home/get-started/get-started.component.html"),
        styles: [__webpack_require__("../../../../../src/app/home/get-started/get-started.component.css")]
    }),
    __metadata("design:paramtypes", [])
], GetStartedComponent);

//# sourceMappingURL=get-started.component.js.map

/***/ }),

/***/ "../../../../../src/app/home/hiw-section/hiw-section.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".hiw {\r\n    padding: 50px 15px;\r\n    background: #00acb80d;\r\n}\r\n.hiw .section-heading {\r\n    font-size: 36px;\r\n    color: #00acb8;\r\n    text-align: center;\r\n    margin-bottom: 30px;\r\n}\r\n.hiw .description p {\r\n    font-size: 16px;\r\n    text-align: justify;\r\n    margin-bottom: 25px;\r\n}\r\n.hiw .video,\r\n.hiw .video-separator {\r\n    float: right;\r\n}\r\n.hiw .video > .hiw-video {\r\n    height: 250px;\r\n    border: solid 5px;\r\n}\r\n#steps {\r\n    margin-top: 15px;\r\n    margin-bottom: 30px;\r\n}\r\n#steps ul {\r\n    padding: 0;\r\n    text-align: center;\r\n}\r\n#steps ul li {\r\n    width: 33%;\r\n    display: inline-block;\r\n    position: relative;\r\n}\r\n.step .number {\r\n    font-size: 28px;\r\n    font-weight: 600;\r\n    border: solid 2px #eee;\r\n    border-radius: 50%;\r\n    text-align: center;\r\n    height: 45px;\r\n    width: 45px;\r\n    margin: 0 auto;\r\n    line-height: 39px;\r\n}\r\n.step .number:after,\r\n.step .number:before {\r\n    content: '';\r\n    position: absolute;\r\n    height: 1px;\r\n    border-top: dashed 2px #ccc;\r\n    top: 22px;\r\n    width: calc(50% - 35px);\r\n    right: 0;\r\n}\r\n.step .number:before {\r\n    left: 0;\r\n}\r\n.step.step1 .number:before {\r\n    display: none;\r\n}\r\n.step.step3 .number:after {\r\n    display: none;\r\n}\r\n.step.step1 .number {\r\n    color: #f2666f;\r\n}\r\n.step.step2 .number {\r\n    color: #fda050;\r\n}\r\n.step.step3 .number {\r\n    color: #8ec486;\r\n}\r\n.step .image {\r\n    margin: 15px auto;\r\n}\r\n.step .step-heading {\r\n    text-transform: uppercase;\r\n    font-weight: bold;\r\n    font-size: 16px;\r\n}\r\n.step .step-description {\r\n    max-width: 225px;\r\n    margin: 0 auto;\r\n    font-size: 16px;\r\n}\r\n\r\n@media screen and (max-width: 1199px) {\r\n    .hiw .section-heading {\r\n        font-size: 32px;\r\n        margin-top: 5px;\r\n        margin-bottom: 5px;\r\n    }\r\n    .hiw .description p {\r\n        margin-bottom: 12px;\r\n    }\r\n}\r\n@media screen and (max-width: 991px) {\r\n    .hiw .section-heading {\r\n        font-size: 20px;\r\n        margin-top: 0px;\r\n        margin-bottom: 5px;\r\n    }\r\n    .hiw .description p {\r\n        font-size: 13px;\r\n        margin-bottom: 7px;\r\n    }\r\n    .hiw .video > .hiw-video {\r\n        height: 200px;\r\n        border: solid 5px;\r\n    }\r\n    .hiw button {\r\n        padding: 7px 10px !important;\r\n        margin-top: 8px;\r\n    }\r\n    .step .step-heading,\r\n    .step .step-description {\r\n        font-size: 15px;\r\n    }\r\n    #steps ul li {\r\n        width: 32%;\r\n    }\r\n}\r\n@media screen and (max-width: 767px) {\r\n    .hiw {\r\n        padding: 35px 15px;\r\n    }\r\n    .hiw .video {\r\n        float: none;\r\n    }\r\n    .hiw .video > .hiw-video {\r\n        max-width: 300px;\r\n        height: 175px;\r\n        margin: 0 auto;\r\n        margin-bottom: 25px;\r\n    }\r\n    .step .step-heading,\r\n    .step .step-description {\r\n        font-size: 13px;\r\n    }\r\n}\r\n@media screen and (max-width: 649px) {\r\n    #steps ul li {\r\n        width: 100%;\r\n        margin-bottom: 25px;\r\n    }\r\n    #steps ul li .number:before,\r\n    #steps ul li .number:after {\r\n        display: none;\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/hiw-section/hiw-section.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"container-fluid hiw\">\r\n    <div class=\"row\">\r\n        <div class=\"container\">\r\n            <h4 class=\"section-heading\" translate=\"hiwsection.heading\"></h4>\r\n            <!-- Three columns of text below the carousel -->\r\n            <div class=\"row\" id=\"steps\">\r\n                <div class=\"col-sm-12\">\r\n                    <ul>\r\n                        <li class=\"step step1\">\r\n                            <div class=\"number\">1</div>\r\n                            <div class=\"image\">\r\n                                <img src=\"../../../assets/images/home/account.png\" alt=\"step1\">\r\n                            </div>\r\n                            <div class=\"step-heading\" translate=\"hiwsection.step1heading\"></div>\r\n                            <div class=\"step-description\" translate=\"hiwsection.step1desc\"></div>\r\n                        </li>\r\n                        <li class=\"step step2\">\r\n                            <div class=\"number\">2</div>\r\n                            <div class=\"image\">\r\n                                <img src=\"../../../assets/images/home/assesment.png\" alt=\"step2\">\r\n                            </div>\r\n                            <div class=\"step-heading\" translate=\"hiwsection.step2heading\"></div>\r\n                            <div class=\"step-description\" translate=\"hiwsection.step2desc\"></div>\r\n                        </li>\r\n                        <li class=\"step step3\">\r\n                            <div class=\"number\">3</div>\r\n                            <div class=\"image\">\r\n                                <img src=\"../../../assets/images/home/track.png\" alt=\"step3\">\r\n                            </div>\r\n                            <div class=\"step-heading\" translate=\"hiwsection.step3heading\"></div>\r\n                            <div class=\"step-description\" translate=\"hiwsection.step3desc\"></div>\r\n                        </li>\r\n                    </ul>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                <div class=\"col-sm-6 video\">\r\n                    <div class=\"hiw-video\">\r\n                        <iframe width=\"100%\" height=\"100%\" src=\"\" data-src=\"https://www.youtube.com/embed/Q8wc53uDM58?rel=0&showinfo=0\" frameborder=\"0\"\r\n                            allowfullscreen></iframe>\r\n                    </div>\r\n                </div>\r\n                <div class=\"col-sm-1 video-separator hidden-xs\">\r\n\r\n                </div>\r\n                <div class=\"col-sm-5 description\">\r\n                    <p translate=\"hiwsection.para1\"></p>\r\n                    <p translate=\"hiwsection.para2\"></p>\r\n                    <button class=\"btn btn-primary custom-small-btn\" routerLink=\"/how-it-works\" translate=\"buttons.learnmore\"></button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/home/hiw-section/hiw-section.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HiwSectionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HiwSectionComponent = (function () {
    function HiwSectionComponent(config) {
        this.config = config;
    }
    HiwSectionComponent.prototype.ngOnInit = function () { };
    HiwSectionComponent.prototype.ngAfterViewInit = function () {
        this.config.deferYoutubeVideo();
    };
    return HiwSectionComponent;
}());
HiwSectionComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-hiw-section',
        template: __webpack_require__("../../../../../src/app/home/hiw-section/hiw-section.component.html"),
        styles: [__webpack_require__("../../../../../src/app/home/hiw-section/hiw-section.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], HiwSectionComponent);

var _a;
//# sourceMappingURL=hiw-section.component.js.map

/***/ }),

/***/ "../../../../../src/app/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".page-scroll > img {\r\n    background: #00acb8;\r\n    border-radius: 50%;\r\n}\r\n\r\n.carousel-image-container {\r\n    display: none;\r\n    text-align: center;\r\n    padding-top: 115px\r\n}\r\n\r\n\r\n@media only screen \r\nand (min-device-width : 768px) \r\nand (max-device-width : 768px) \r\nand (orientation : portrait) { \r\n    .carousel-image-container {\r\n        display: block;\r\n    }\r\n    .carousel-image-container > img{\r\n        max-height: 325px;\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<header class=\"carousel slide top-slider\" id=\"myCarousel\">\r\n    <!-- Indicators -->\r\n    <ol class=\"carousel-indicators\">\r\n        <li class=\"active\" data-slide-to=\"0\" data-target=\"#myCarousel\"></li>\r\n        <li data-slide-to=\"1\" data-target=\"#myCarousel\" class=\"\"></li>\r\n        <li data-slide-to=\"2\" data-target=\"#myCarousel\" class=\"\"></li>\r\n    </ol>\r\n\r\n    <!-- Wrapper for Slides -->\r\n    <div class=\"carousel-inner \">\r\n        <div class=\"item active\">\r\n            <div class=\"container\">\r\n                <div class=\"carousel-image-container container-1\">\r\n                    <img src=\"../../assets/images/home/depression.png\" alt=\"\">\r\n                </div>\r\n                <div class=\"carousel-caption\">\r\n                    <h2>\r\n                        <span class=\"blue-color-heading\" translate=\"homepage.slide1blueheading\"></span><br>\r\n                    </h2>\r\n                    <p translate=\"homepage.slide1description1\"></p>\r\n                    <p translate=\"homepage.slide1description2\"></p>\r\n                    <a class=\"btn btn-lg btn-primary hvr-float-shadow\" routerLink=\"/depression\" translate=\"buttons.learnmore\"></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"item\">\r\n            <div class=\"container\">\r\n                <div class=\"carousel-image-container container-2\">\r\n                        <img src=\"../../assets/images/home/stress.png\" alt=\"\">\r\n                    </div>\r\n                <div class=\"carousel-caption\">\r\n                    <h2>\r\n                        <span class=\"blue-color-heading\" translate=\"homepage.slide2blueheading\"></span><br>\r\n                    </h2>\r\n                    <p translate=\"homepage.slide2description1\"></p>\r\n                    <p translate=\"homepage.slide2description2\"></p>\r\n                    <a class=\"btn btn-lg btn-primary hvr-float-shadow\" routerLink=\"/stress\" translate=\"buttons.starttoday\"></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"item\">\r\n            <div class=\"container\">\r\n                <div class=\"carousel-image-container container-3\">\r\n                        <img src=\"../../assets/images/home/memory.png\" alt=\"\">\r\n                    </div>\r\n                <div class=\"carousel-caption\">\r\n                    <h2>\r\n                        <span class=\"blue-color-heading\" translate=\"homepage.slide3blueheading\"></span><br>\r\n                    </h2>\r\n                    <p translate=\"homepage.slide3description1\"></p>\r\n                    <p translate=\"homepage.slide3description2\"></p>\r\n                    <a class=\"btn btn-lg btn-primary hvr-float-shadow\" [href]=\"config.appDomain+'psychoterapy'\" target=\"_blank\" translate=\"buttons.conductmemoryanalysis\"></a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <p class=\"move-down\">\r\n        <a class=\"page-scroll\" [attr.data-target]=\"'.brain9d'\">\r\n            <img src=\"../../assets/images/scroll_down.png\" />\r\n        </a>\r\n    </p>\r\n\r\n</header>\r\n\r\n<!-- what is brain9d section -->\r\n<app-brain9d-section></app-brain9d-section>\r\n\r\n<!-- Marketing messaging and featurettes -->\r\n<app-hiw-section></app-hiw-section>\r\n\r\n<div class=\"clearfix\"></div>\r\n\r\n<!-- Get started section -->\r\n<app-get-started></app-get-started>\r\n\r\n<!-- <app-testimonials></app-testimonials> -->\r\n\r\n<div class=\"clearfix\"></div>\r\n\r\n<!-- Post Add -->\r\n<app-post-ad></app-post-ad>\r\n<!-- Post Add End -->\r\n\r\n<!-- <script type=\"text/javascript\">\r\n    $(document).ready(function() {\r\n        //carousel options\r\n        $('#myCarousel').carousel({\r\n            pause: true,\r\n            interval: 8000,\r\n        });\r\n        $('#quote-carousel').carousel({\r\n            pause: true,\r\n            interval: 10000,\r\n        });\r\n        $('a').tooltip();\r\n    });\r\n</script> -->"

/***/ }),

/***/ "../../../../../src/app/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = (function () {
    function HomeComponent(http, config) {
        this.http = http;
        this.config = config;
        // Initializations
        this.showLoader = true;
        this.feedbacks = [];
    }
    HomeComponent.prototype.ngOnInit = function () {
        $('#myCarousel').carousel({
            pause: true,
            interval: 6000,
        });
        this.showLoader = true;
        this.getUserFeedback();
        $("a.page-scroll").click(function () {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('data-target')).offset().top
            }, 500);
        });
    };
    HomeComponent.prototype.getUserFeedback = function () {
        var _this = this;
        this.http.get(this.config.apiDomain + '/feedback?ipp=6&&fbk_status=publish&&expand=feedbackUser')
            .subscribe(function (response) {
            _this.showLoader = false;
            _this.feedbacks = response.json().map(function (feedback) {
                var domain = 'https://api.dev.brain9d.com/uploads/';
                if (feedback.feedbackUser && feedback.feedbackUser.usr_profile_picture)
                    feedback.fbk_image = domain + feedback.feedbackUser.usr_profile_picture;
                else
                    feedback.fbk_image = "../assets/images/feed_back.png";
                return feedback;
            });
        }, function (error) { return console.log(error); });
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-home',
        template: __webpack_require__("../../../../../src/app/home/home.component.html"),
        styles: [__webpack_require__("../../../../../src/app/home/home.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__config_service__["a" /* ConfigService */]) === "function" && _b || Object])
], HomeComponent);

var _a, _b;
//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ "../../../../../src/app/home/post-ad/post-ad.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#ads-cta {\r\n    background: #00ddec;\r\n    padding: 30px 15px;\r\n}\r\n\r\n#ads-cta h3 {\r\n    margin: 0;\r\n    color: #fff;\r\n    height: 36px;\r\n    line-height: 36px;\r\n}\r\n\r\n.btnc {\r\n    text-align: right;\r\n}\r\n\r\n.btnc>a {\r\n    color: #00acb8;\r\n    font-size: 16px;\r\n    text-transform: uppercase;\r\n    padding: 5px 25px;\r\n    border: solid 2px #fff;\r\n    text-decoration: none;\r\n    background: #fff;\r\n    box-shadow: 0 0 15px;\r\n    display: inline-block;\r\n    text-align: center;\r\n}\r\n\r\n.btnc>a:hover {\r\n    background: transparent;\r\n    color: #fff;\r\n}\r\n\r\n@media screen and ( max-width: 991px) {\r\n    #ads-cta h3 {\r\n        height: auto;\r\n        line-height: 30px;\r\n    }\r\n    .btnc>a {\r\n        margin-top: 12px;\r\n    }\r\n}\r\n\r\n@media screen and ( max-width: 767px) {\r\n    #ads-cta h3,\r\n    .btnc {\r\n        text-align: center\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/post-ad/post-ad.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"ads-cta\" class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"container add-ad\">\r\n            <div class=\"col-sm-8 col-md-9\">\r\n                <h3 translate=\"messages.promotebrand\"></h3>\r\n            </div>\r\n            <div class=\"col-sm-4 col-md-3 btnc\">\r\n                <a [href]=\"config.appDomain + 'signup'\" target=\"_blank\" translate=\"buttons.postanad\"></a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/home/post-ad/post-ad.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostAdComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PostAdComponent = (function () {
    function PostAdComponent(config) {
        this.config = config;
    }
    PostAdComponent.prototype.ngOnInit = function () {
    };
    return PostAdComponent;
}());
PostAdComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["o" /* Component */])({
        selector: 'app-post-ad',
        template: __webpack_require__("../../../../../src/app/home/post-ad/post-ad.component.html"),
        styles: [__webpack_require__("../../../../../src/app/home/post-ad/post-ad.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], PostAdComponent);

var _a;
//# sourceMappingURL=post-ad.component.js.map

/***/ }),

/***/ "../../../../../src/app/home/testimonials/testimonials.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/testimonials/testimonials.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container testimonial-crousal\">\r\n    <h2><span class=\"blue-color-heading\" translate=\"homepage.testimonialsheading\"></span></h2>\r\n    <p class=\"feedback-detail\"><span translate=\"homepage.testimonialsdescriptionline1\"></span><br> <span translate=\"homepage.testimonialsdescriptionline2\"></span> </p>\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <div class=\"carousel slide\" data-ride=\"carousel\" id=\"quote-carousel\">\r\n  \r\n          <!-- Bottom Carousel Indicators -->\r\n          <ol class=\"carousel-indicators\">\r\n            <li *ngIf=\"feedbacks.length>0\" data-target=\"#quote-carousel\" data-slide-to=\"0\" class=\"active\"></li>\r\n            <li *ngIf=\"feedbacks.length>2\" data-target=\"#quote-carousel\" data-slide-to=\"1\"></li>\r\n            <li *ngIf=\"feedbacks.length>4\" data-target=\"#quote-carousel\" data-slide-to=\"2\"></li>\r\n          </ol>\r\n  \r\n          <!-- Carousel Slides / Quotes -->\r\n          <div class=\"carousel-inner\">\r\n  \r\n            <!-- Quote 1 -->\r\n            <div *ngIf=\"feedbacks.length>0\" class=\"item active\">\r\n              <div class=\"row\">\r\n                <div *ngFor=\"let fb of feedbacks.slice(0,2)\" class=\"col-sm-6\">\r\n                  <div class=\"col-sm-1\"></div>\r\n                  <div class=\"col-sm-11  bg-testimonial\">\r\n                    <img [src]=\"fb.fbk_image\">\r\n                    <h3>{{fb.feedbackUser.usr_first_name}} {{fb.feedbackUser.usr_last_name}}</h3>\r\n                    <div class=\"feedback-date\"> {{fb.fbk_created_at.replace(\" \", \"T\")+\".000Z\" | date : \"medium\"}} </div>\r\n                    <p class=\"message\" ng-if=\"fb.fbk_feedback_message\" >\"{{fb.fbk_feedback_message.slice(0,175)}} <span ng-if=\"fb.fbk_feedback_message.length>175\">...</span>\"</p>\r\n                    <p class=\"ratings\">\r\n                      <i class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 2}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 3}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 4}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 5}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n  \r\n            <!-- Quote 2 -->\r\n            <div *ngIf=\"feedbacks.length>2\" class=\"item\">\r\n              <div class=\"row\">\r\n                <div *ngFor=\"let fb of feedbacks.slice(2,4)\" class=\"col-sm-6\">\r\n                  <div class=\"col-sm-1\"></div>\r\n                  <div class=\"col-sm-11  bg-testimonial\">\r\n                    <img [src]=\"fb.fbk_image\">\r\n                    <h3>{{fb.feedbackUser.usr_first_name}} {{fb.feedbackUser.usr_last_name}}</h3>\r\n                    <div class=\"feedback-date\"> {{fb.fbk_created_at.replace(\" \", \"T\")+\".000Z\" | date : \"medium\"}} </div>\r\n                    <p class=\"message\" ng-if=\"fb.fbk_feedback_message\" >\"{{fb.fbk_feedback_message.slice(0,175)}} <span ng-if=\"fb.fbk_feedback_message.length>175\">...</span>\"</p>\r\n                    <p class=\"ratings\">\r\n                      <i class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 2}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 3}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 4}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 5}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n  \r\n            <!-- Quote 3 -->\r\n            <div *ngIf=\"feedbacks.length>4\" class=\"item\">\r\n              <div class=\"row\">\r\n                <div *ngFor=\"let fb of feedbacks.slice(4,6)\" class=\"col-sm-6\">\r\n                  <div class=\"col-sm-1\"></div>\r\n                  <div class=\"col-sm-11  bg-testimonial\">\r\n                    <img [src]=\"fb.fbk_image\">\r\n                    <h3>{{fb.feedbackUser.usr_first_name}} {{fb.feedbackUser.usr_last_name}}</h3>\r\n                    <div class=\"feedback-date\"> {{fb.fbk_created_at.replace(\" \", \"T\")+\".000Z\" | date : \"medium\"}} </div>\r\n                    <p class=\"message\" ng-if=\"fb.fbk_feedback_message\" >\"{{fb.fbk_feedback_message.slice(0,175)}} <span ng-if=\"fb.fbk_feedback_message.length>175\">...</span>\"</p>\r\n                    <p class=\"ratings\">\r\n                      <i class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 2}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 3}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 4}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                      <i [ngClass]=\"{'rating-light': fb.fbk_star_rating < 5}\" class=\"fa fa-star\" aria-hidden=\"true\"></i>\r\n                    </p>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <!-- end Quote 3 -->\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "../../../../../src/app/home/testimonials/testimonials.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestimonialsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TestimonialsComponent = (function () {
    function TestimonialsComponent(http) {
        this.http = http;
        // Initializations
        this.showLoader = true;
        this.feedbacks = [];
    }
    TestimonialsComponent.prototype.ngOnInit = function () {
        this.showLoader = true;
        // this.getUserFeedback();
    };
    return TestimonialsComponent;
}());
TestimonialsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-testimonials',
        template: __webpack_require__("../../../../../src/app/home/testimonials/testimonials.component.html"),
        styles: [__webpack_require__("../../../../../src/app/home/testimonials/testimonials.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object])
], TestimonialsComponent);

var _a;
//# sourceMappingURL=testimonials.component.js.map

/***/ }),

/***/ "../../../../../src/app/how-it-works-result/how-it-works-result.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/how-it-works-result/how-it-works-result.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- main body section -->\r\n\r\n\r\n<div class=\"main-content-area container\">\r\n\t<h3 class=\"faq-heading\" translate=\"howitworksresult.heading\"></h3>\r\n\r\n\t<p class=\"note-content\" style=\"text-align: center;\">\r\n\t\t<span translate=\"howitworksresult.notecontent1\"></span>\r\n\t\t<br>\r\n\t\t<span translate=\"howitworksresult.notecontent2\"></span>\r\n\t</p>\r\n\t<div class=\" col-md-6 col-md-offset-3\">\r\n\t\t<div class=\"body bg-cyan col-md-12\">\r\n\t\t\t<h3 style=\"padding:0 15px;\" translate=\"howitworksresult.graphheading\"></h3>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\" col-md-6 col-md-offset-3\">\r\n\t\t<div class=\"body bg-cyan col-md-12\">\r\n\t\t\t<h3 translate=\"howitworksresult.graph\"></h3>\r\n\t\t\t<i><img src=\"../assets/images/graph.png\" style=\"width: 90%;\"></i>\r\n\t\t</div>\r\n\t\t<br><br>\r\n\t\t<p class=\"form-sub-heading1\" translate=\"howitworksresult.note\"></p>\r\n\t</div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/how-it-works-result/how-it-works-result.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HowItWorksResultComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HowItWorksResultComponent = (function () {
    function HowItWorksResultComponent() {
    }
    HowItWorksResultComponent.prototype.ngOnInit = function () {
    };
    return HowItWorksResultComponent;
}());
HowItWorksResultComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-how-it-works-result',
        template: __webpack_require__("../../../../../src/app/how-it-works-result/how-it-works-result.component.html"),
        styles: [__webpack_require__("../../../../../src/app/how-it-works-result/how-it-works-result.component.css")]
    }),
    __metadata("design:paramtypes", [])
], HowItWorksResultComponent);

//# sourceMappingURL=how-it-works-result.component.js.map

/***/ }),

/***/ "../../../../../src/app/how-it-works/how-it-works.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".hiw-page {\r\n    padding: 50px 15px;\r\n}\r\n.hiw-page .section-heading {\r\n    font-size: 32px;\r\n    text-transform: uppercase;\r\n    color: #00acb8;\r\n    font-weight: 600;\r\n    text-align: center;\r\n    margin-bottom: 30px;\r\n}\r\n.hiw-page .video {\r\n    float: right;\r\n}\r\n.hiw-page .video > .hiw-video {\r\n    height: 250px;\r\n}\r\n#steps {\r\n    margin-top: 15px;\r\n    margin-bottom: 30px;\r\n}\r\n#steps ul {\r\n    padding: 0;\r\n    text-align: center;\r\n}\r\n#steps ul li {\r\n    width: 33%;\r\n    display: inline-block;\r\n    position: relative;\r\n}\r\n.step .number {\r\n    font-size: 28px;\r\n    font-weight: 600;\r\n    border: solid 2px #eee;\r\n    border-radius: 50%;\r\n    text-align: center;\r\n    height: 45px;\r\n    width: 45px;\r\n    margin: 0 auto;\r\n    line-height: 39px;\r\n}\r\n.step .number:after,\r\n.step .number:before {\r\n    content: '';\r\n    position: absolute;\r\n    height: 1px;\r\n    border-top: dashed 2px #ccc;\r\n    top: 22px;\r\n    width: calc(50% - 35px);\r\n    right: 0;\r\n}\r\n.step .number:before {\r\n    left: 0;\r\n}\r\n.step.step1 .number:before {\r\n    display: none;\r\n}\r\n.step.step3 .number:after {\r\n    display: none;\r\n}\r\n.step.step1 .number {\r\n    color: #f2666f;\r\n}\r\n.step.step2 .number {\r\n    color: #fda050;\r\n}\r\n.step.step3 .number {\r\n    color: #8ec486;\r\n}\r\n.step .image {\r\n    margin: 15px auto;\r\n}\r\n.step .step-heading {\r\n    text-transform: uppercase;\r\n    font-weight: bold;\r\n    font-size: 16px;\r\n}\r\n.step .step-description {\r\n    max-width: 225px;\r\n    margin: 0 auto;\r\n    font-size: 16px;\r\n}\r\nul.hiw-text {\r\n    padding-top: 7px;\r\n    padding-right: 20px;\r\n}\r\nul.hiw-text li {\r\n    list-style:none;\r\n}\r\n ul.hiw-text .hiw-text-heading {\r\n    position: relative;\r\n    margin-bottom: 0px;\r\n    color: #00acb8;\r\n    font-weight: bold;\r\n }\r\n ul.hiw-text .hiw-text-heading i {\r\n    position: absolute;\r\n    left: -20px;\r\n    top: 2px;\r\n    font-size: 17px;\r\n }\r\n ul.hiw-text .hiw-text-desc {\r\n     margin-bottom: 3px;\r\n }\r\n .links {\r\n     padding: 0;\r\n     margin-top: 20px;\r\n }\r\n .links .link {\r\n     background: #f3f3f3;\r\n     text-align: center;\r\n     display: block;\r\n     padding: 15px;\r\n     margin-bottom: 20px;\r\n     text-decoration: none;\r\n }\r\n .links .link .link-desc {\r\n     max-width: 75%;\r\n     margin: 0 auto;\r\n     color: #000;\r\n     min-height: 40px;\r\n }\r\n .links .link .link-heading {\r\n    text-transform: uppercase;\r\n    letter-spacing: 0.5px;\r\n    font-weight: 600;\r\n }\r\n .link.pink .link-heading {\r\n    color: #ef414b;\r\n }\r\n .link.orange .link-heading {\r\n    color: #f6892a;\r\n }\r\n .link.green .link-heading {\r\n    color: #5dab46;\r\n }\r\n .link.blue .link-heading {\r\n    color: #4f76bb;\r\n }\r\n@media screen and (max-width: 1199px) {\r\n    .links .link .link-desc {\r\n        max-width: 80%;\r\n    }\r\n}\r\n@media screen and (max-width: 991px) {\r\n    .step .step-heading,\r\n    .step .step-description {\r\n        font-size: 15px;\r\n    }\r\n    #steps ul li {\r\n        width: 32%;\r\n    }\r\n    ul.hiw-text {\r\n        padding-left: 15px;\r\n        padding-top: 0;\r\n    }\r\n    ul.hiw-text .hiw-text-heading {\r\n        font-size: 13px;\r\n        line-height: 15px;\r\n    }\r\n    ul.hiw-text .hiw-text-heading i {\r\n        left: -15px;\r\n        top: 1px;\r\n        font-size: 13px;\r\n    }\r\n    ul.hiw-text .hiw-text-desc {\r\n        font-size: 13px;\r\n        line-height: 17px;\r\n    }\r\n    .links .link .link-desc {\r\n        max-width: 90%;\r\n        font-size: 12px;\r\n    }\r\n}\r\n@media screen and (max-width: 767px) {\r\n    .step .step-heading,\r\n    .step .step-description {\r\n        font-size: 13px;\r\n    }\r\n    .hiw-page .video {\r\n        float: none;\r\n        max-width: 450px;\r\n        margin: 0 auto;\r\n        margin-bottom: 40px;\r\n    }\r\n    ul.hiw-text .hiw-text-heading {\r\n        font-size: 14px;\r\n        line-height: 25px;\r\n    }\r\n    ul.hiw-text .hiw-text-heading i {\r\n        left: -15px;\r\n        top: 6px;\r\n        font-size: 14px;\r\n    }\r\n    ul.hiw-text .hiw-text-desc {\r\n        font-size: 14px;\r\n        line-height: 20px;\r\n    }\r\n}\r\n@media screen and (max-width: 649px) {\r\n    #steps ul li {\r\n        width: 100%;\r\n        margin-bottom: 25px;\r\n    }\r\n    #steps ul li .number:before,\r\n    #steps ul li .number:after {\r\n        display: none;\r\n    }\r\n}\r\n@media screen and (max-width: 480px) {\r\n    .hiw-page .video > .hiw-video {\r\n        height: 160px; \r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/how-it-works/how-it-works.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- main body section -->\r\n<div class=\"container hiw-page\">\r\n    <h4 class=\"section-heading\" translate=\"hiwsection.heading\"></h4>\r\n    \r\n    <!-- Three columns of text below the carousel -->\r\n    <div class=\"row\" id=\"steps\">\r\n        <div class=\"col-sm-12\">\r\n            <ul>\r\n                <li class=\"step step1\">\r\n                    <div class=\"number\">1</div>\r\n                    <div class=\"image\">\r\n                        <img src=\"../../../assets/images/home/account.png\" alt=\"step1\">\r\n                    </div>\r\n                    <div class=\"step-heading\" translate=\"hiwsection.step1heading\"></div>\r\n                    <div class=\"step-description\" translate=\"hiwsection.step1desc\"></div>\r\n                </li>\r\n                <li class=\"step step2\">\r\n                    <div class=\"number\">2</div>\r\n                    <div class=\"image\">\r\n                        <img src=\"../../../assets/images/home/assesment.png\" alt=\"step2\">\r\n                    </div>\r\n                    <div class=\"step-heading\" translate=\"hiwsection.step2heading\"></div>\r\n                    <div class=\"step-description\" translate=\"hiwsection.step2desc\"></div>\r\n                </li>\r\n                <li class=\"step step3\">\r\n                    <div class=\"number\">3</div>\r\n                    <div class=\"image\">\r\n                        <img src=\"../../../assets/images/home/track.png\" alt=\"step3\">\r\n                    </div>\r\n                    <div class=\"step-heading\" translate=\"hiwsection.step3heading\"></div>\r\n                    <div class=\"step-description\" translate=\"hiwsection.step3desc\"></div>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <p class=\"para text-justify\" translate=\"hiwpage.description1\"></p>\r\n            <p class=\"para text-justify\" translate=\"hiwpage.description2\"></p>\r\n        </div>\r\n    </div>\r\n\r\n    <br>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-6 col-md-5 video\">\r\n            <div class=\"hiw-video\">\r\n                <iframe width=\"100%\" height=\"100%\" src=\"\" data-src=\"https://www.youtube.com/embed/Q8wc53uDM58?rel=0&amp;showinfo=0\" frameborder=\"0\" allowfullscreen></iframe>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-6 col-md-7 points\">\r\n            <ul class=\"list-disc hiw-text\">\r\n                <li>\r\n                    <p class=\"hiw-text-heading\"><i class=\"fa fa-check-circle\"></i><span translate=\"hiwpage.questionnaires\"></span></p>\r\n                    <p class=\"hiw-text-desc\" translate=\"hiwpage.questionnairesdesc\"></p>\r\n                </li>\r\n                <li>\r\n                    <p class=\"hiw-text-heading\"><i class=\"fa fa-check-circle\"></i><span translate=\"hiwpage.personalmedicalhistory\"></span></p>\r\n                    <p class=\"hiw-text-desc\" translate=\"hiwpage.personalmedicalhistorydesc\"></p>\r\n                </li>\r\n                <li>\r\n                    <p class=\"hiw-text-heading\"><i class=\"fa fa-check-circle\"></i><span translate=\"hiwpage.lifestylehabbits\"></span></p>\r\n                    <p class=\"hiw-text-desc\" translate=\"hiwpage.lifestylehabbitsdesc\"></p>\r\n                </li>\r\n                <li>\r\n                    <p class=\"hiw-text-heading\"><i class=\"fa fa-check-circle\"></i><span translate=\"hiwpage.facialrecording\"></span></p>\r\n                    <p class=\"hiw-text-desc\" translate=\"hiwpage.facialrecordingdesc\"></p>\r\n                </li>\r\n                <li>\r\n                    <p class=\"hiw-text-heading\"><i class=\"fa fa-check-circle\"></i><span translate=\"hiwpage.brainsignalrecordings\"></span></p>\r\n                    <p class=\"hiw-text-desc\" translate=\"hiwpage.brainsignalrecordingsdesc\"></p>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <br><br>\r\n            <p class=\"para text-justify\" translate=\"hiwpage.description3\"></p>\r\n        </div>\r\n        <div class=\"col-sm-12 links\">\r\n            <div class=\"col-sm-6\">\r\n                <a [href]=\"config.appDomain+'hospitals'\" target=\"_blank\" class=\"link pink\">\r\n                    <img src=\"../../assets/images/hiw/doctor.png\" alt=\"find a doc\"/>\r\n                    <h4 class=\"link-heading\" translate=\"hiwpage.findadoc\"></h4>\r\n                    <p class=\"link-desc\" translate=\"hiwpage.findadocdesc\"></p>\r\n                </a>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n                <a [href]=\"config.appDomain+'support-groups'\" target=\"_blank\" class=\"link orange\">\r\n                    <img src=\"../../assets/images/hiw/support.png\" alt=\"support\"/>\r\n                    <h4 class=\"link-heading\" translate=\"hiwpage.supportgroups\"></h4>\r\n                    <p class=\"link-desc\" translate=\"hiwpage.supportgroupsdesc\"></p>\r\n                </a>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n                <a routerLink=\"/brain-shop\" class=\"link blue\">\r\n                    <img src=\"../../assets/images/hiw/shop.png\" alt=\"brain shop\"/>\r\n                    <h4 class=\"link-heading\" translate=\"hiwpage.brainshop\"></h4>\r\n                    <p class=\"link-desc\" translate=\"hiwpage.brainshopdesc\"></p>\r\n                </a>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n                <a [href]=\"config.appDomain+'brain-training'\" target=\"_blank\" class=\"link green\">\r\n                    <img src=\"../../assets/images/hiw/training.png\" alt=\"brain training\"/>\r\n                    <h4 class=\"link-heading\" translate=\"hiwpage.braintraining\"></h4>\r\n                    <p class=\"link-desc\" translate=\"hiwpage.braintrainingdesc\"></p>\r\n                </a>\r\n            </div>\r\n        </div>\r\n        <div class=\"clearfix\"></div>\r\n    </div>\r\n    <br>\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12 text-center\">\r\n            <a class=\"btn custom-small-btn btn-primary hvr-float-shadow\" [href]=\"config.appDomain+'signup'\" target=\"_blank\" translate=\"buttons.createfreeaccount\"></a>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"main-content-area container\">\r\n\r\n    <div style=\"box-shadow: 0 5px 14px rgba(0,0,0,0.3); padding: 15px 15px 1px 15px;\">\r\n        <h3 class=\"faq-heading\" style=\"color:#00acb8;padding-bottom:0;padding-top:10px;\" translate=\"howitworksform.heading\"></h3>\r\n        <p class=\"text-center\" translate=\"howitworksform.subtext\"></p>\r\n        <li class=\"question inner-form\">\r\n            <span translate=\"howitworksform.question1\"></span>\r\n            <ul class=\"choose-answer \">\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio6\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.veryoften\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n          <input name=\"radio6\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.notatall\"></i>\r\n        </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio6\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.quiteoften\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio6\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.occasionally\"></i>\r\n          </label>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n        <li class=\"question inner-form\">\r\n            <span translate=\"howitworksform.question2\"></span>\r\n            <ul class=\"choose-answer \">\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.veryoften\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.notatall\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.quiteoften\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.occasionally\"></i>\r\n          </label>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n        <li class=\"question inner-form\">\r\n            <span translate=\"howitworksform.question3\"></span>\r\n            <ul class=\"choose-answer \">\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio1\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.veryoften\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio1\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.notatall\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio1\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.quiteoften\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio1\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.occasionally\"></i>\r\n          </label>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n        <li class=\"question inner-form\">\r\n            <span translate=\"howitworksform.question4\"></span>\r\n            <ul class=\"choose-answer \">\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio2\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.veryoften\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio2\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.notatall\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio2\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.quiteoften\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio2\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.occasionally\"></i>\r\n          </label>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n        <li class=\"question inner-form\">\r\n            <span translate=\"howitworksform.question5\"></span>\r\n            <ul class=\"choose-answer \">\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio3\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.veryoften\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio3\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.notatall\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio3\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.quiteoften\"></i>\r\n          </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n            <input name=\"radio3\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.occasionally\"></i>\r\n          </label>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n        <li class=\"question inner-form\">\r\n            <span translate=\"howitworksform.question6\"></span>\r\n            <ul class=\"choose-answer \">\r\n                <li>\r\n                    <label>\r\n                      <input name=\"radio4\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.veryoften\"></i>\r\n                    </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n                      <input name=\"radio4\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.notatall\"></i>\r\n                    </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n                      <input name=\"radio4\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.quiteoften\"></i>\r\n                    </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n                      <input name=\"radio4\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.occasionally\"></i>\r\n                    </label>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n        <li class=\"question inner-form\">\r\n            <span translate=\"howitworksform.question7\"></span>\r\n            <ul class=\"choose-answer \">\r\n                <li>\r\n                    <label>\r\n                      <input name=\"radio5\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.veryoften\"></i>\r\n                    </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n                      <input name=\"radio5\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.notatall\"></i>\r\n                    </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n                        <input name=\"radio5\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.quiteoften\"></i>\r\n                    </label>\r\n                </li>\r\n                <li>\r\n                    <label>\r\n                      <input name=\"radio5\" value=\"1\" type=\"radio\" checked> <span></span> <i translate=\"howitworksform.occasionally\"></i>\r\n                    </label>\r\n                </li>\r\n            </ul>\r\n        </li>\r\n        <p class=\"text-center clearfix\">\r\n            <a class=\"btn custom-small-btn btn-primary hvr-float-shadow\" role=\"button\" routerLink=\"/how-it-works-result\" data-original-title=\"\" title=\"\" style=\"width: 171px;\" translate=\"buttons.submit\"></a>\r\n        </p>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/how-it-works/how-it-works.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HowItWorksComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HowItWorksComponent = (function () {
    function HowItWorksComponent(config) {
        this.config = config;
    }
    HowItWorksComponent.prototype.ngOnInit = function () {
    };
    HowItWorksComponent.prototype.ngAfterViewInit = function () {
        this.config.deferYoutubeVideo();
    };
    return HowItWorksComponent;
}());
HowItWorksComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-how-it-works',
        template: __webpack_require__("../../../../../src/app/how-it-works/how-it-works.component.html"),
        styles: [__webpack_require__("../../../../../src/app/how-it-works/how-it-works.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], HowItWorksComponent);

var _a;
//# sourceMappingURL=how-it-works.component.js.map

/***/ }),

/***/ "../../../../../src/app/inner-loader/inner-loader.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#inner-loader-container {\r\n    position: fixed;\r\n    top: -20px;\r\n    bottom: -56px;\r\n    width: 100%;\r\n    z-index: 9999;\r\n    background: rgba(0,0,0,0.7);\r\n  }\r\n  .inner-loader {\r\n    top: 50%;\r\n    left: 50%;\r\n    position: absolute;\r\n    -webkit-transform: translateX(-50%)translateY(-50%);\r\n    transform: translateX(-50%)translateY(-50%);\r\n  }", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/inner-loader/inner-loader.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"inner-loader-container\" ng-if=\"showLoader\">\r\n    <div class=\"inner-loader inner-loader-style1\" title=\"0\">\r\n        <svg version=\"1.1\" id=\"loader-1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\" width=\"40px\" height=\"40px\" viewBox=\"0 0 40 40\" enable-background=\"new 0 0 40 40\" xml:space=\"preserve\">\r\n          <path opacity=\"0.5\" fill=\"#fff\" d=\"M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946\r\n          s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634\r\n          c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z\"></path>\r\n          <path fill=\"#00acb8\" d=\"M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0\r\n          C22.32,8.481,24.301,9.057,26.013,10.047z\">\r\n            <animateTransform attributeType=\"xml\"\r\n            attributeName=\"transform\"\r\n            type=\"rotate\"\r\n            from=\"0 20 20\"\r\n            to=\"360 20 20\"\r\n            dur=\"1s\"\r\n            repeatCount=\"indefinite\"></animateTransform>\r\n          </path>\r\n        </svg>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/inner-loader/inner-loader.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InnerLoaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InnerLoaderComponent = (function () {
    function InnerLoaderComponent() {
    }
    InnerLoaderComponent.prototype.ngOnInit = function () {
    };
    return InnerLoaderComponent;
}());
InnerLoaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-inner-loader',
        template: __webpack_require__("../../../../../src/app/inner-loader/inner-loader.component.html"),
        styles: [__webpack_require__("../../../../../src/app/inner-loader/inner-loader.component.css")]
    }),
    __metadata("design:paramtypes", [])
], InnerLoaderComponent);

//# sourceMappingURL=inner-loader.component.js.map

/***/ }),

/***/ "../../../../../src/app/learning-center/learning-center.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/learning-center/learning-center.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-area\">\r\n  <div class=\"container-fluid user-account-container\">\r\n    <div class=\"row\">\r\n        \r\n      <div id=\"externalContainer\" class=\"text-center\">\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n  "

/***/ }),

/***/ "../../../../../src/app/learning-center/learning-center.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LearningCenterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LearningCenterComponent = (function () {
    function LearningCenterComponent(config) {
        this.config = config;
    }
    LearningCenterComponent.prototype.ngOnInit = function () {
        jQuery("#externalContainer").load(this.config.externalLinksUrl + "learningcenter/index.php");
    };
    return LearningCenterComponent;
}());
LearningCenterComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["o" /* Component */])({
        selector: 'app-learning-center',
        template: __webpack_require__("../../../../../src/app/learning-center/learning-center.component.html"),
        styles: [__webpack_require__("../../../../../src/app/learning-center/learning-center.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], LearningCenterComponent);

var _a;
//# sourceMappingURL=learning-center.component.js.map

/***/ }),

/***/ "../../../../../src/app/main-loader/main-loader.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".scene-container {\r\n    position: fixed;\r\n    width: 100%;\r\n    height: 100%;\r\n    z-index: 9999999;\r\n    background: #333;\r\n  }\r\n  .scene {\r\n    width: 100%;\r\n    height: 100%;\r\n    -webkit-perspective: 600;\r\n            perspective: 600;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    -webkit-box-pack: center;\r\n        -ms-flex-pack: center;\r\n            justify-content: center;\r\n  }\r\n  .scene svg {\r\n    width: 150px;\r\n    height: 150px;\r\n  }\r\n  \r\n  @-webkit-keyframes arrow-spin {\r\n    50% {\r\n      -webkit-transform: rotateY(360deg);\r\n              transform: rotateY(360deg);\r\n    }\r\n  }\r\n  \r\n  @keyframes arrow-spin {\r\n    50% {\r\n      -webkit-transform: rotateY(360deg);\r\n              transform: rotateY(360deg);\r\n    }\r\n  }", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-loader/main-loader.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"scene-container\">\r\n    <div class=\"scene\">\r\n      <svg version=\"1.1\" id=\"dc-spinner\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"38\" height=\"38\" viewBox=\"0 0 38 38\" preserveAspectRatio=\"xMinYMin meet\">\r\n        <text x=\"14\" y=\"21\" font-family=\"Monaco\" font-size=\"2px\" style=\"letter-spacing:0.6\" fill=\"grey\">LOADING\r\n            <animate attributeName=\"opacity\" values=\"0;1;0\" dur=\"1.8s\" repeatCount=\"indefinite\"></animate>\r\n        </text>\r\n        <path fill=\"#373a42\" d=\"M20,35c-8.271,0-15-6.729-15-15S11.729,5,20,5s15,6.729,15,15S28.271,35,20,35z M20,5.203\r\n            C11.841,5.203,5.203,11.841,5.203,20c0,8.159,6.638,14.797,14.797,14.797S34.797,28.159,34.797,20\r\n            C34.797,11.841,28.159,5.203,20,5.203z\">\r\n        </path>\r\n\r\n        <path fill=\"#373a42\" d=\"M20,33.125c-7.237,0-13.125-5.888-13.125-13.125S12.763,6.875,20,6.875S33.125,12.763,33.125,20\r\n            S27.237,33.125,20,33.125z M20,7.078C12.875,7.078,7.078,12.875,7.078,20c0,7.125,5.797,12.922,12.922,12.922\r\n            S32.922,27.125,32.922,20C32.922,12.875,27.125,7.078,20,7.078z\">\r\n        </path>\r\n\r\n        <path fill=\"#00acb8\" stroke=\"#00acb8\" stroke-width=\"0.6027\" stroke-miterlimit=\"10\" d=\"M5.203,20\r\n          c0-8.159,6.638-14.797,14.797-14.797V5C11.729,5,5,11.729,5,20s6.729,15,15,15v-0.203C11.841,34.797,5.203,28.159,5.203,20z\">\r\n          <animateTransform \r\n            attributeName=\"transform\" \r\n            type=\"rotate\" \r\n            from=\"0 20 20\" \r\n            to=\"360 20 20\" \r\n            calcMode=\"spline\" \r\n            keySplines=\"0.4, 0, 0.2, 1\" \r\n            keyTimes=\"0;1\" \r\n            dur=\"2s\" \r\n            repeatCount=\"indefinite\">\r\n          </animateTransform>\r\n        </path>\r\n\r\n        <path fill=\"#ffc354\" stroke=\"#ffc354\" stroke-width=\"0.2027\" stroke-miterlimit=\"10\" d=\"M7.078,20\r\n        c0-7.125,5.797-12.922,12.922-12.922V6.875C12.763,6.875,6.875,12.763,6.875,20S12.763,33.125,20,33.125v-0.203\r\n        C12.875,32.922,7.078,27.125,7.078,20z\">\r\n            <animateTransform \r\n              attributeName=\"transform\" \r\n              type=\"rotate\" \r\n              from=\"0 20 20\" \r\n              to=\"360 20 20\" \r\n              dur=\"1.8s\" \r\n              repeatCount=\"indefinite\">\r\n            </animateTransform>\r\n        </path>\r\n      </svg>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/main-loader/main-loader.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainLoaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainLoaderComponent = (function () {
    function MainLoaderComponent() {
    }
    MainLoaderComponent.prototype.ngOnInit = function () {
    };
    return MainLoaderComponent;
}());
MainLoaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-main-loader',
        template: __webpack_require__("../../../../../src/app/main-loader/main-loader.component.html"),
        styles: [__webpack_require__("../../../../../src/app/main-loader/main-loader.component.css")]
    }),
    __metadata("design:paramtypes", [])
], MainLoaderComponent);

//# sourceMappingURL=main-loader.component.js.map

/***/ }),

/***/ "../../../../../src/app/membership/membership.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".membership {\r\n    text-align: center;\r\n}\r\n.membership img {\r\n    display: block;\r\n    margin: 30px auto;\r\n}\r\n.membership .note {\r\n    font-size: 16px;\r\n    font-weight: bold;\r\n    text-transform: uppercase;\r\n}\r\np > a.custom-small-btn {\r\n    min-width: 200px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/membership/membership.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container membership\">\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12\">\r\n      <h3 class=\"faq-heading\" translate=\"membershippage.heading\"></h3>\r\n      <h4 class=\"note\" translate=\"membershippage.note\"></h4>\r\n      <p class=\"para\" translate=\"membershippage.para1\"></p>\r\n      <img class=\"\" src=\"../../assets/images/membership/face.png\" alt=\"membership\" />\r\n      <p>\r\n        <a class=\"btn custom-small-btn btn-primary hvr-float-shadow\" [href]=\"config.appDomain+'signup'\" target=\"_blank\" translate=\"buttons.signupnow\"></a>\r\n      </p>\r\n      <br><br>\r\n      <p class=\"text-justify para\" translate=\"membershippage.para2\"></p>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/membership/membership.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MembershipComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MembershipComponent = (function () {
    function MembershipComponent(config) {
        this.config = config;
    }
    MembershipComponent.prototype.ngOnInit = function () {
    };
    return MembershipComponent;
}());
MembershipComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-membership',
        template: __webpack_require__("../../../../../src/app/membership/membership.component.html"),
        styles: [__webpack_require__("../../../../../src/app/membership/membership.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], MembershipComponent);

var _a;
//# sourceMappingURL=membership.component.js.map

/***/ }),

/***/ "../../../../../src/app/sliderpages/depression-and-anxiety/depression-and-anxiety.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "p.link {\r\n    margin-bottom: 5px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/sliderpages/depression-and-anxiety/depression-and-anxiety.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content-area container\">\r\n  <h3 class=\"faq-heading\" translate=\"depressionpage.heading\"></h3>\r\n  <p translate=\"depressionpage.para1\"></p>\r\n  <p translate=\"depressionpage.para2\"></p>\r\n  <p translate=\"depressionpage.para3\"></p>\r\n  <p translate=\"depressionpage.para4\"></p>\r\n  <p translate=\"depressionpage.para5\"></p>\r\n  <p class=\"link\">\r\n    <a [href]=\"config.appDomain + 'form?form_id=124'\" target=\"_blank\" translate=\"depressionpage.link1\"></a>\r\n  </p>\r\n  <p class=\"link\">  \r\n    <a [href]=\"config.appDomain + 'form?form_id=88'\" target=\"_blank\" translate=\"depressionpage.link2\"></a>\r\n  </p>\r\n  <!-- <p class=\"link\">  \r\n    <a [href]=\"\" target=\"_blank\" translate=\"depressionpage.link3\"></a>\r\n  </p> -->\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/sliderpages/depression-and-anxiety/depression-and-anxiety.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DepressionAndAnxietyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DepressionAndAnxietyComponent = (function () {
    function DepressionAndAnxietyComponent(config) {
        this.config = config;
    }
    DepressionAndAnxietyComponent.prototype.ngOnInit = function () {
    };
    return DepressionAndAnxietyComponent;
}());
DepressionAndAnxietyComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-depression-and-anxiety',
        template: __webpack_require__("../../../../../src/app/sliderpages/depression-and-anxiety/depression-and-anxiety.component.html"),
        styles: [__webpack_require__("../../../../../src/app/sliderpages/depression-and-anxiety/depression-and-anxiety.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], DepressionAndAnxietyComponent);

var _a;
//# sourceMappingURL=depression-and-anxiety.component.js.map

/***/ }),

/***/ "../../../../../src/app/sliderpages/memory-loss/memory-loss.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "p.link {\r\n    margin-bottom: 5px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/sliderpages/memory-loss/memory-loss.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content-area container\">\r\n  <h3 class=\"faq-heading\" translate=\"memorylosspage.heading\"></h3>\r\n  <p translate=\"memorylosspage.para1\"></p>\r\n  <p translate=\"memorylosspage.para2\"></p>\r\n  <p translate=\"memorylosspage.para3\"></p>\r\n  <p translate=\"memorylosspage.para4\"></p>\r\n  <p translate=\"memorylosspage.para5\"></p>\r\n  <p class=\"link\">\r\n    <a [href]=\"config.appDomain + 'form?form_id=152'\" target=\"_blank\" translate=\"memorylosspage.link1\"></a>\r\n  </p>\r\n  <p class=\"link\">\r\n    <a [href]=\"config.appDomain + 'psychotherapy'\" target=\"_blank\" translate=\"memorylosspage.link2\"></a>\r\n  </p>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/sliderpages/memory-loss/memory-loss.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MemoryLossComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MemoryLossComponent = (function () {
    function MemoryLossComponent(config) {
        this.config = config;
    }
    MemoryLossComponent.prototype.ngOnInit = function () {
    };
    return MemoryLossComponent;
}());
MemoryLossComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-memory-loss',
        template: __webpack_require__("../../../../../src/app/sliderpages/memory-loss/memory-loss.component.html"),
        styles: [__webpack_require__("../../../../../src/app/sliderpages/memory-loss/memory-loss.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], MemoryLossComponent);

var _a;
//# sourceMappingURL=memory-loss.component.js.map

/***/ }),

/***/ "../../../../../src/app/sliderpages/stress/stress.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".sub-heading {\r\n    font-weight: 600;\r\n    font-style: italic;\r\n    margin-bottom: 30px;\r\n    font-size: 24px;\r\n}\r\n.sub-heading:before, .sub-heading:after {\r\n    content: '\"';\r\n}\r\np.link {\r\n    margin-bottom: 5px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/sliderpages/stress/stress.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content-area container\">\r\n  <h3 class=\"faq-heading\" translate=\"stresspage.heading\"></h3>\r\n  <h4 class=\"sub-heading text-center\" translate=\"stresspage.subheading\"></h4>\r\n  <p translate=\"stresspage.para1\"></p>\r\n  <p translate=\"stresspage.para2\"></p>\r\n  <p translate=\"stresspage.para3\"></p>\r\n  <p translate=\"stresspage.para4\"></p>\r\n  <p translate=\"stresspage.para5\"></p>\r\n  <p class=\"link\"><a [href]=\"config.appDomain + 'form?form_id=165'\" target=\"_blank\" translate=\"stresspage.link1\"></a></p>\r\n  <!-- <p class=\"link\"><a [href]=\"\" target=\"_blank\" translate=\"stresspage.link2\"></a></p> -->\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/sliderpages/stress/stress.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StressComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config_service__ = __webpack_require__("../../../../../src/app/config.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StressComponent = (function () {
    function StressComponent(config) {
        this.config = config;
    }
    StressComponent.prototype.ngOnInit = function () {
    };
    return StressComponent;
}());
StressComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-stress',
        template: __webpack_require__("../../../../../src/app/sliderpages/stress/stress.component.html"),
        styles: [__webpack_require__("../../../../../src/app/sliderpages/stress/stress.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__config_service__["a" /* ConfigService */]) === "function" && _a || Object])
], StressComponent);

var _a;
//# sourceMappingURL=stress.component.js.map

/***/ }),

/***/ "../../../../../src/app/terms-and-conditions/terms-and-conditions.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#euContent p{\r\n    margin-bottom: 10px;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/terms-and-conditions/terms-and-conditions.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- main body section -->\r\n<div class=\"main-content-area container terms-and-conditions\">\r\n    <h3 class=\"faq-heading\" translate=\"termsandconditionspage.heading\"></h3>\r\n    <p class=\"note-content\" translate=\"termsandconditionspage.note\"></p>\r\n    <ul>\r\n        <li><a class=\"note-content\" id=\"anchorEU\" [attr.data-target]=\"'#europeanUnion'\">EU GDPR</a></li>\r\n        <li translate=\"termsandconditionspage.point1\"></li>\r\n        <li translate=\"termsandconditionspage.point2\"></li>\r\n        <li translate=\"termsandconditionspage.point3\"></li>\r\n        <li translate=\"termsandconditionspage.point4\"></li>\r\n        <li translate=\"termsandconditionspage.point5\"></li>\r\n        <li translate=\"termsandconditionspage.point6\"></li>\r\n        <li translate=\"termsandconditionspage.point7\"></li>\r\n        <li translate=\"termsandconditionspage.point8\"></li>\r\n        <li translate=\"termsandconditionspage.point9\"></li>\r\n        <li translate=\"termsandconditionspage.point10\"></li>\r\n        <li translate=\"termsandconditionspage.point11\"></li>\r\n        <li translate=\"termsandconditionspage.point12\"></li>\r\n        <li translate=\"termsandconditionspage.point13\"></li>\r\n        <li translate=\"termsandconditionspage.point14\"></li>\r\n        <li translate=\"termsandconditionspage.point15\"></li>\r\n        <li translate=\"termsandconditionspage.point16\"></li>\r\n        <li translate=\"termsandconditionspage.point17\"></li>\r\n        <li translate=\"termsandconditionspage.point18\"></li>\r\n        <li translate=\"termsandconditionspage.point19\"></li>\r\n        <li translate=\"termsandconditionspage.point20\"></li>\r\n        <li translate=\"termsandconditionspage.point21\"></li>\r\n        <li translate=\"termsandconditionspage.point22\"></li>\r\n        <li translate=\"termsandconditionspage.point23\"></li>\r\n        <li translate=\"termsandconditionspage.point24\"></li>\r\n        <li translate=\"termsandconditionspage.point25\"></li>\r\n        <li translate=\"termsandconditionspage.point26\"></li>\r\n        <li translate=\"termsandconditionspage.point27\"></li>\r\n        <li translate=\"termsandconditionspage.point28\"></li>\r\n        <li translate=\"termsandconditionspage.point29\"></li>\r\n        <li translate=\"termsandconditionspage.point30\"></li>\r\n        <li translate=\"termsandconditionspage.point31\"></li>\r\n        <li translate=\"termsandconditionspage.point32\"></li>\r\n        <li translate=\"termsandconditionspage.point33\"></li>\r\n        <li translate=\"termsandconditionspage.point34\"></li>\r\n        <li translate=\"termsandconditionspage.point35\"></li>\r\n\r\n        <ul>\r\n            <li translate=\"termsandconditionspage.point36\"></li>\r\n            <ul>\r\n                <li translate=\"termsandconditionspage.point37\"></li>\r\n                <li translate=\"termsandconditionspage.point38\"></li>\r\n                <li translate=\"termsandconditionspage.point39\"></li>\r\n                <li translate=\"termsandconditionspage.point40\"></li>\r\n                <li translate=\"termsandconditionspage.point41\"></li>\r\n                <li translate=\"termsandconditionspage.point42\"></li>\r\n                <li translate=\"termsandconditionspage.point43\"></li>\r\n                <li translate=\"termsandconditionspage.point44\"></li>\r\n            </ul>\r\n        </ul>\r\n        <li translate=\"termsandconditionspage.point45\"></li>\r\n        <li translate=\"termsandconditionspage.point46\"></li>\r\n        <li translate=\"termsandconditionspage.point47\"></li>\r\n        <li translate=\"termsandconditionspage.point48\"></li>\r\n        <li translate=\"termsandconditionspage.point49\"></li>\r\n        <li translate=\"termsandconditionspage.point50\"></li>\r\n        <li translate=\"termsandconditionspage.point51\"></li>\r\n        <li translate=\"termsandconditionspage.point52\"></li>\r\n        <li translate=\"termsandconditionspage.point53\"></li>\r\n        <li translate=\"termsandconditionspage.point54\"></li>\r\n        <li translate=\"termsandconditionspage.point55\"></li>\r\n        <li translate=\"termsandconditionspage.point56\"></li>\r\n        <li translate=\"termsandconditionspage.point57\"></li>\r\n        <li translate=\"termsandconditionspage.point58\"></li>\r\n        <li translate=\"termsandconditionspage.point59\"></li>\r\n    </ul>\r\n    <div id=\"euContent\">\r\n        <p class=\"note-content\" id=\"europeanUnion\" translate=\"termsandconditionspage.europeanUnion.heading\"></p>\r\n        <p translate=\"termsandconditionspage.europeanUnion.para1\"></p>\r\n        <p translate=\"termsandconditionspage.europeanUnion.para2\"></p>\r\n        <p translate=\"termsandconditionspage.europeanUnion.para3\"></p>\r\n        <p translate=\"termsandconditionspage.europeanUnion.note1\"></p>\r\n        <ul>\r\n            <li translate=\"termsandconditionspage.europeanUnion.point1\"></li>\r\n            <li translate=\"termsandconditionspage.europeanUnion.point2\"></li>\r\n        </ul>\r\n        <p translate=\"termsandconditionspage.europeanUnion.note2\"></p>\r\n        <ul>\r\n            <li translate=\"termsandconditionspage.europeanUnion.point3\"></li>\r\n            <li translate=\"termsandconditionspage.europeanUnion.point4\"></li>\r\n            <li translate=\"termsandconditionspage.europeanUnion.point5\"></li>\r\n            <ul>\r\n                <li translate=\"termsandconditionspage.europeanUnion.point5a\"></li>\r\n                <li translate=\"termsandconditionspage.europeanUnion.point5b\"></li>\r\n            </ul>\r\n        </ul>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/terms-and-conditions/terms-and-conditions.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TermsAndConditionsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TermsAndConditionsComponent = (function () {
    function TermsAndConditionsComponent(route, router) {
        this.route = route;
        this.router = router;
    }
    TermsAndConditionsComponent.prototype.ngOnInit = function () {
        $("a#anchorEU").click(function () {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('data-target')).offset().top
            }, 500);
        });
    };
    return TermsAndConditionsComponent;
}());
TermsAndConditionsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-terms-and-conditions',
        template: __webpack_require__("../../../../../src/app/terms-and-conditions/terms-and-conditions.component.html"),
        styles: [__webpack_require__("../../../../../src/app/terms-and-conditions/terms-and-conditions.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["f" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["f" /* Router */]) === "function" && _b || Object])
], TermsAndConditionsComponent);

var _a, _b;
//# sourceMappingURL=terms-and-conditions.component.js.map

/***/ }),

/***/ "../../../../../src/assets/images/ads-badge.png":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAACdCAMAAAB8So3NAAABjFBMVEUAAABTw8tYtLttz9ZrzdNmx85jxMppytFgwMZdvMMWlZ1auL9Gn6ZjusA2paxOsLdQq7FduL1fuL8Lj5g9qLBIrbVTs7kfmaMxoalBq7LW7/Gd3eFUtbtzvMBxusALjJXj9PUurrconqUln6c7payz5OdNucFZxs54yc5puL5Js7t0z9ZMw8xHvcUAqrYAipMArLgAqbQAo68An6oAmqUAp7IAkp0AnKcAoawAjpgAjJUApbEAmKMAfIUAkJoAp7MAl6IAlqAAcnoAnagAlZ8ApbAAh5AAbHMAoq0AlJ4AkZsAhY4AhI0AgosAfocAaXEAZ24AjZcAeYIAd4AAdX4AbnbMzMz7+/tNoqn////Ozs74+Pjs7OyzxMVmq7Du7u7e3t6As7fp6enS0tJAnqUakpvb29uavL9ysbXh4eHz8/PV1dXAyMmmwMKNt7qzs7OsrKylpaXw8PBapqwzmqLL3t/Y2NiKyc0yrbWgoKAMlp8Njpb9/f3Dy8y2x8h0wsdXuL+3t7dRq7Izm6LFdisyAAAALnRSTlMAwHBwcHBwcHBw+3BwH9yccFc8/s+vh/bpvu7YkkoQ/vTv7+vi4MCvZGRkPC4u/jYETgAAA4dJREFUaN7t2vlTEmEcx/G1PBIDAhXP7L7LzRPvOw86vR5WCRJFwAOPtLLs7h+PJ/f5ysNCA/T97owbrxl+cEY/b2XXHYVVyD28U5LU6G9sKsFzy3+1qaT0gZ54dLvH+/3Du9UjL5qj5Nw3r/faXT3h+uTz+X76UMX5pq9ZLzgniVxpOQm0DFK5pJwYo3JRD0xREYFXVETgJRXpKfo4hQb2RGCR219EA3siMMbtj6GBPSmwhxzYSwkscHsLaGBPBOa53Xk0sCd+k+e43Tk0sCcCgxR+JB8QIAKBF1RE4DkVEXhGRQSeUjmnByapiIBKRQow5HGWEujjlvvQwJ4I9HJrvWhgryo1sIUc2EoJDHFbQ2hgTwS6uffdaGCvMjUQRQ5EUwITFKLJhwiMUrmgB0aoiMAAFRHopEIeKNcDXiqmBca51XE0sCcCHdzrDjSwRx4o0wM93JseNLBnWmCaezuNBvZEoJ3bbEcDexAAyIEKPfCEigj0UxGBLirWCQxTKdUDbVSkQAB5PJAe8CMH/JYKtHL+VjSwd95aAWyfW4kCoBj4vwKPqUiBJeTxpWKgGEAOcGctwFnnWiQ+LgayBtpIWOpv0zaO8v+DYS4wjAb2TAt0cYEuNLBXYVagn8Jm8gEBKqa92tJOhTxQZpkAvEqIzTovzJK9dn32A+XUAdPeARkHyAHrvAvVSeXsB8jfjZUCGwP4pDesN0bQwJ5pgVFuZRQN7JkWmOBWJtDAXqVZgRkuOoMG9iAAiALdVKwT8FERgSEqVeQBorvUQJVZt8HNcmuzaGCvivpOwfwD2tcv8fXcA+fyDixzMS3XQCF3a/4KxVg417s1cw2sh7Y1VVJAILvDMGMsqBpth9YRAlqCz8cjmSYYCx+qWeUYCLOdgyzfaCQeZCyhFRwwstVctjtvSEfnYIflG4DjqsqqHR7lD3etTZXkFYDjGpR/9HqnXTnlclQbjlVQyzmgJdLmbbVuJZ2nXpUEGXxR5oAWOz2u0mdW17iUjOy1ddIRh0TmQCjzwbrpUf7CXWPLdsS1mCEQCcdUWR088dl5HH2qEd8zBBjbkZ74BreSE7vzuuE3JKbFDYGEdpx6Tt5T8uBusMmBbcbSA/I5qeTHeOZGwlkDdbV2pTDN0pl7nBaAi4FSOPnMlQJwMfhnrhqbMQAXAxwehyEAFwMkdmedFICLAQo4cyFA5b7D9RvgAYQ0XieG+gAAAABJRU5ErkJggg=="

/***/ }),

/***/ "../../../../../src/assets/images/ads-bg.jpg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ads-bg.001e87abe4fb40d0cd9c.jpg";

/***/ }),

/***/ "../../../../../src/assets/images/brain9d/note.png":
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAA3CAYAAACPdVOuAAAACXBIWXMAAAsSAAALEgHS3X78AAABw0lEQVRo3u2Z0ZGDIBRFLcESUoIl8L8/lmAJlEAJKYESKIESKMEOlg5YmXnssAwYlKeQLB93MmMycoLvXS4yGGOGEn2TL7ZJbzKgdRMtva9VKRj3oEI9q8FtgxMPxM7eCPKBp1pwDoJHvpMOuhZcEsA+0tpwzDVAcH30GoTeAmfrB4Cc/Nri3nWRuG5FLTwq3HbDZacrj8rO6gMTbkWEy67FXDhTDQ7qiUMHxoQNt+6MxVxdhmbaipQFvKKesESHRsGs5JCY0hket5NGHpgH918iY0ThxoTPYQHKxBj0FZzEyGEIKafDXQsX1JsujUFocNC1u8tPxuoSEw+b4gycSC0/CKFAlsIl11WMUFAKl9pdicwdmMndnZ2BGyPmq8NdFdQdOaAJq1tHcG8BcebRfe4TTJh4lmK7d27FhKdEp81BTbKDoleasPJ+o05aifiXJqxy/sCrNIxRczqj5ujBmluwunWC2pPwSbrPdbgbYzrKi+i7Y7o4GNNFJHY1FdNVyyuEviOmP2vG9PUdYjrrMb3DXQCnWoYrPoZEOEvbhdPQneQmzQk/lS2/TRfDRSc0GFqcyarGwPifgznIbawB/e5RfgCEDNdI05qj4wAAAABJRU5ErkJggg=="

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map