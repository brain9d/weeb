var server = "brain9d";
if(window.location.origin.indexOf('dev.') > -1 || window.location.origin.indexOf('localhost') > -1)
	server = "development";

var apiDomain = "https://api.brain9d.com/v1";
var appDomain = "https://app.brain9d.com/";
var facebookAppId = "216915902095824";
var forumUrl = "https://brain9d.com/forum/index.php";
var androidAppUrl = "https://play.google.com/store/apps/details?id=com.brain9d.brainhealth";
var iphoneAppUrl = "https://itunes.apple.com/us/app/brain9d/id1176918637?mt=8";
var externalLinksUrl = "https://brain9d.com/";
var twoCheckoutSid = '103041325';
var pushNotificationTopic = 'brain9d';

if(server == "development") {
	apiDomain = "https://api.dev.brain9d.com/v1";
	appDomain = "https://app.dev.brain9d.com/";
	facebookAppId = "217392898670282";
	forumUrl = "https://dev.brain9d.com/forum/index.php";
	twoCheckoutSid = '901341537';
	pushNotificationTopic = 'brain9d_dev';
}

if(window.location.origin.indexOf('localhost') > -1)
	facebookAppId = "370978729918129";

var googleAppId = "990135941036-v886ogicnltqaj6hssssk50vls7v0lcb.apps.googleusercontent.com";
var isNativeApp = navigator.userAgent.match(/B9DNativeApp/i) //Detecting Application

var currentVersion=1505457612661;